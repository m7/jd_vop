<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 RefundWaresInfos
 */
class OrderRefundCompletedRefundWaresInfos
{
    /**
     * @var int|mixed
     */
    public $skuId;
    /**
     * @var mixed|string
     */
    public $skuName;
    /**
     * @var int|mixed
     */
    public $wareNumber;

    /**
     * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 RefundWaresInfos
     */
    public function __construct($result)
    {
        $this->skuId = $result['skuId'] ?? 0;
        $this->skuName = $result['skuName'] ?? "";
        $this->wareNumber = $result['wareNumber'] ?? 0;
    }
}