<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 49 折扣率变更消息
 * 此消息开通需要走线下邮件开通申请。
 */
class DiscountRateChange
{

    /**
     * @var float
     */
    public $oldRate;
    /**
     * @var string
     */
    public $category;
    /**
     * @var float
     */
    public $newRate;


    /**
     * 11.1 查询推送信息 Result 49 折扣率变更消息
     * 此消息开通需要走线下邮件开通申请。
     * @param $result
     */
    public function __construct($result)
    {
        $this->category = $result['category'] ?? "";
        $this->oldRate = $result['oldRate'] ?? 0;
        $this->newRate = $result['newRate'] ?? 0;
    }
}