<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 100 商品税率变更消息（目前未涵盖全部商品）
 * 客户侧取outputVAT作为采购税率
 * 此消息开通需要走线下邮件开通申请。
 */
class ProductTaxRateChange
{
    /**
     * @var string
     */
    public $timestampLong;
    /**
     * @var int
     */
    public $sku_id;
    /**
     * @var string
     */
    public $features;

    /**
     * 11.1 查询推送信息 Result 100 商品税率变更消息（目前未涵盖全部商品）
     * 客户侧取outputVAT作为采购税率
     * 此消息开通需要走线下邮件开通申请。
     * @param $result
     */
    public function __construct($result)
    {
        $this->sku_id = $result['sku_id'] ?? 0;
        $this->timestampLong = $result['timestampLong'] ?? "";
        $this->features = $result['features'] ?? "";
    }
}