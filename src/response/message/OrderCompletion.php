<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 31 订单完成消息
 */
class OrderCompletion
{

    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var int 订单状态
     */
    public $jdOrderState;
    /**
     * @var string 完成时间
     */
    public $completeTime;


    /**
     * 11.1 查询推送信息 Result 31 订单完成消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
        $this->jdOrderState = $result['jdOrderState'] ?? 0;
        $this->pin = $result['pin'] ?? "";
        $this->completeTime = $result['completeTime'] ?? "";
    }
}