<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 48 商品池添加、删除消息
 */
class ProductPoolChange
{

    /**
     * @var string 京东订单编号
     * poolType:
     * p_skupool 用户的私有商品池；
     * cate_pool 分类商品池
     * recommend 主推商品池；hot_sale 热销商品池；
     * p_custom_skupool 用户的私有定制商品池；
     */
    public $poolType;
    /**
     * @var string 商品池编号
     */
    public $page_num;
    /**
     * @var int 状态
     * 1添加，2删除
     */
    public $state;



    /**
     * 11.1 查询推送信息 Result 48 商品池添加、删除消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->poolType = $result['poolType'] ?? "";
        $this->state = $result['state'] ?? 0;
        $this->page_num = $result['page_num'] ?? "";
    }
}