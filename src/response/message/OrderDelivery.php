<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 5 订单妥投消息
 * 该订单已妥投（买断模式代表外单已妥投或外单已拒收）。
 */
class OrderDelivery
{
    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var int 状态
     * 1是妥投，2是拒收
     */
    public $state;

    /**
     * 11.1 查询推送信息 Result 5 订单妥投消息
     * 该订单已妥投（买断模式代表外单已妥投或外单已拒收）。
     * @param $result
     */
    public function __construct($result)
    {
        $this->state = $result['state'] ?? 0;
        $this->orderId = $result['orderId'] ?? 0;
    }
}