<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 15 7天未支付取消消息/未确认取消消息
 */
class UnconfirmedCancel
{

    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var int 取消类型
     * cancelType, 1: 7天未支付取消消息; 2: 未确认取消
     */
    public $cancelType;

    /**
     * 11.1 查询推送信息 Result 15 7天未支付取消消息/未确认取消消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
        $this->cancelType = $result['cancelType'] ?? 0;
    }
}