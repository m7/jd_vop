<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 RefundPaymentInfos
 */
class OrderRefundCompletedRefundPaymentInfos
{
//kaPayFlag 混合⽀付必填 （个⼈/企业）
    /**
     * @var int|mixed b2bPayType 混合⽀付必填（B中台的⽀付类型） ；
     */
    public $b2bPayType;
    /**
     * @var int|mixed
     */
    public $id;
    /**
     * @var int|mixed payEnum 商品类型；
     */
    public $payEnum;
    /**
     * @var mixed|string payId ⽀付单号；
     */
    public $payId;
    /**
     * @var int|mixed
     */
    public $payTime;
    /**
     * @var int|mixed payType ⽀付类型；
     */
    public $payType;
    /**
     * @var int|mixed
     */
    public $refundId;
    /**
     * @var int|mixed refundSourceId 退款明细单编号；
     */
    public $refundSourceId;
    /**
     * @var int|mixed refundableAmount 退款业务占⽤本⽀付单的⾦额；
     */
    public $refundableAmount;

    /**
     * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 RefundPaymentInfos
     */
    public function __construct($result)
    {
        $this->b2bPayType = $result['b2bPayType'] ?? 0;
        $this->id = $result['id'] ?? 0;
        $this->payEnum = $result['payEnum'] ?? 0;
        $this->payId = $result['payId'] ?? "";
        $this->payTime = $result['payTime'] ?? 0;
        $this->payType = $result['payType'] ?? 0;
        $this->refundId = $result['refundId'] ?? 0;
        $this->refundSourceId = $result['refundSourceId'] ?? 0;
        $this->refundableAmount = $result['refundableAmount'] ?? 0;
    }
}