<?php

namespace jd_vop\response\message;

use jd_vop\response\Result;

/**
 * 11.1 查询推送信息 Result
 */
class Get implements Result
{
    /**
     * @var int 消息ID
     */
    public $id;
    /**
     * @var int 消息类型
     */
    public $type;
    /**
     * @var mixed 消息内容
     */
    public $result;
    /**
     * @var string 消息时间
     */
    public $time;

    /**
     * @param int $id 消息ID
     * @param int $type 消息类型
     * @param mixed $result 消息内容
     * @param string $time 消息时间
     */
    public function __construct(int $id, int $type, $result, string $time)
    {
        $this->id = $id;
        $this->type = $type;
        $this->result = $result;
        $this->time = $time;
    }


    /**
     * @param $result
     * @return self
     */
    public static function parse($result): self
    {
        $type = $result['type'] ?? 0;
        switch ($type) {
            case 1:
                /**
                 * 订单拆分消息
                 */
                $data = new OrderSplit($result['result']);
                break;
            case 2:
                /**
                 * 商品价格变更
                 */
                $data = new PriceChange($result['result']);
                break;
            case 4:
                /**
                 * 商品上下架变更消息
                 */
                $data = new ProductStatusChange($result['result']);
                break;
            case 5:
                /**
                 * 订单妥投消息
                 */
                $data = new OrderDelivery($result['result']);
                break;
            case 6:
                /**
                 * 商品池内商品添加、删除消息
                 */
                $data = new ProductPoolProductChange($result['result']);
                break;
            case 10:
                /**
                 * 订单取消消息
                 */
                $data = new OrderCancel($result['result']);
                break;
            case 11:
                /**
                 * 发票开票进度消息
                 */
                $data = new InvoicingProgress($result['result']);
                break;
            case 12:
                /**
                 * 配送单生成成功消息
                 */
                $data = new DistributionOrderGeneration($result['result']);
                break;
            case 14:
                /**
                 * 支付失败消息
                 */
                $data = new PayFail($result['result']);
                break;
            case 15:
                /**
                 * 7天未支付取消消息/未确认取消消息
                 */
                $data = new UnconfirmedCancel($result['result']);
                break;
            case 16:
                /**
                 * 商品信息变更
                 */
                $data = new ProductInfoChange($result['result']);
                break;
            case 17:
                /**
                 * 赠品促销变更消息
                 */
                $data = new ProductGiftChange($result['result']);
                break;
            case 18:
                /**
                 * 订单等待确认收货消息
                 */
                $data = new OrderWaitConfirm($result['result']);
                break;
            case 23:
                /**
                 * 订单配送退货消息
                 */
                $data = new OrderReturn($result['result']);
                break;
            case 25:
                /**
                 * 新订单消息
                 */
                $data = new NewOrder($result['result']);
                break;
            case 26:
                /**
                 * 预定订单消息
                 */
                $data = new AdvanceOrder($result['result']);
                break;
            case 28:
                /**
                 * 旧售后服务单状态变更
                 */
                $data = new ServiceOrderChangeOld($result['result']);
                break;
            case 31:
                /**
                 * 订单完成消息
                 */
                $data = new OrderCompletion($result['result']);
                break;
            case 48:
                /**
                 * 商品池添加、删除消息
                 */
                $data = new ProductPoolChange($result['result']);
                break;
            case 49:
                /**
                 * 折扣率变更消息
                 */
                $data = new DiscountRateChange($result['result']);
                break;
            case 50:
                /**
                 * 京东地址变更消息
                 */
                $data = new JdAddressChange($result['result']);
                break;
            case 100:
                /**
                 * 商品税率变更消息（目前未涵盖全部商品）
                 */
                $data = new ProductTaxRateChange($result['result']);
                break;
            case 102:
                /**
                 * 专票资质审核进度消息
                 */
                $data = new InvoiceExamineProcess($result['result']);
                break;
            case 104:
                /**
                 * 申请单环节变更消息
                 */
                $data = new ApplicationFormLinkChange($result['result']);
                break;
            case 105:
                /**
                 * 订单维度售后完成
                 */
                $data = new OrderAfterSalesCompletion($result['result']);
                break;
            case 108:
                /**
                 * 混合支付微信个人支付成功消息
                 */
                $data = new MixedPaymentWechatPaySuccess($result['result']);
                break;
            case 117:
                /**
                 * 电子合同消息
                 */
                $data = new ElectronicContract($result['result']);
                break;
            case 119:
                /**
                 * 申请单维度售后退款完成消息
                 */
                $data = new OrderRefundCompleted($result['result']);
                break;
            case 126:
                /**
                 * 商品可售状态变更
                 */
                $data = new ProductSellStateChange($result['result']);
                break;
            default:
                $data = $result['result'];
                break;
        }
        return new self(
            $result['id'] ?? 0,
            $type,
            $data,
            $result['time'] ?? ""
        );
    }

}