<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 25 旧售后服务单状态变更
 */
class ServiceOrderChangeOld
{
    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var int 商品编号
     */
    public $skuId;
    /**
     * @var bool 是否线上申请
     * isOffline":false, 客户接口申请 ；
     * isOffline":true 京东客服代提交 ；
     */
    public $isOffline;

    /**
     * @var int 服务单状态
     * state状态：
     * 1：创建；
     * 2：审核不通过；
     * 3：审核取消；
     * 4：完成
     * 5：待用户确认（调用服务单确认接口使服务单状态从用户确认状态变成完成）
     */
    public $state;
    /**
     * @var string 服务单号
     */
    public $afsServiceId;


    /**
     * 11.1 查询推送信息 Result 25 旧售后服务单状态变更
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
        $this->skuId = $result['skuId'] ?? 0;
        $this->state = $result['state'] ?? 0;
        $this->pin = $result['pin'] ?? "";
        $this->afsServiceId = $result['afsServiceId'] ?? "";
        $this->isOffline = $result['isOffline'] ?? false;
    }
}