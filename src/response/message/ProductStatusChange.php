<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 4 商品上下架变更消息
 */
class ProductStatusChange
{

    /**
     * @var int 状态
     * state:1代表在主站（jd.com）上架；0代表下架
     */
    public $state;
    /**
     * @var int 商品编号
     */
    public $skuId;
    /**
     * @param $result
     */
    public function __construct($result)
    {
        $this->state = $result['state'] ?? 0;
        $this->skuId = $result['skuId'] ?? 0;
    }
}