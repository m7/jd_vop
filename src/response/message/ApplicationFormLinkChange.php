<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 104 申请单环节变更消息
 */
class ApplicationFormLinkChange
{


    /**
     * @var bool
     */
    public $expectationChanged;
    /**
     * @var string
     */
    public $thirdApplyId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var int
     * stepPassType:环节通过情况
     * 10, “全部通过”
     * 20, “部分通过”
     * 30, “没有通过”
     */
    public $stepPassType;
    /**
     * @var bool
     */
    public $isOffline;
    /**
     * @var int
     * 10:申请
     * 20:审核
     * 30:收货
     * 40:处理
     * 50:待用户确认
     * 60:完成
     * 70:取消
     * 环节在20或40的时候才会有类型变更
     * applyStep为20或40的时候stepPassType有值
     */
    public $applyStep;
    /**
     * @var string
     */
    public $contractNumber;
    /**
     * @var int
     */
    public $orderId;

    /**
     * 11.1 查询推送信息 Result 104 申请单环节变更消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->expectationChanged = $result['expectationChanged'] ?? false;
        $this->isOffline = $result['isOffline'] ?? false;
        $this->thirdApplyId = $result['thirdApplyId'] ?? "";
        $this->pin = $result['pin'] ?? "";
        $this->stepPassType = $result['stepPassType'] ?? 0;
        $this->contractNumber = $result['contractNumber'] ?? "";
        $this->applyStep = $result['applyStep'] ?? 0;
        $this->orderId = $result['orderId'] ?? 0;
    }
}