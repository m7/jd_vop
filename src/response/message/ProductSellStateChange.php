<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 126 商品可售状态变更
 * 商品可售状态变更消息，接收到消息后，需反查可售(https://bizapi.jd.com/api/product/check)，获取最新商品可售状态
 * 目前仅支持商品预约预售变动，其它可售变动不发出此消息
 */
class ProductSellStateChange
{

    /**
     * @var int 商品编号
     */
    public $skuId;


    /**
     * 11.1 查询推送信息 Result 126 商品可售状态变更
     * 商品可售状态变更消息，接收到消息后，需反查可售(https://bizapi.jd.com/api/product/check)，获取最新商品可售状态
     * 目前仅支持商品预约预售变动，其它可售变动不发出此消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->skuId = $result['skuId'] ?? 0;
    }
}