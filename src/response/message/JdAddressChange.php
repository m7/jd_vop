<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 50 京东地址变更消息
 * 此消息开通需要走线下邮件开通申请。
 */
class JdAddressChange
{
    /**
     * @var int 京东地址编码
     */
    public $areaId;
    /**
     * @var string 京东地址名称
     */
    public $areaName;
    /**
     * @var int 父京东ID编码
     */
    public $parentId;
    /**
     * @var int 地址等级(行政级别：国家(1)、省(2)、市(3)、县(4)、镇(5))
     */
    public $areaLevel;
    /**
     * @var int 操作类型(插入数据为1，更新时为2，删除时为3)
     */
    public $operateType;

    /**
     * 11.1 查询推送信息 Result 50 京东地址变更消息
     * 此消息开通需要走线下邮件开通申请。
     */
    public function __construct($result)
    {
        $this->areaId = $result['areaId'] ?? 0;
        $this->areaName = $result['areaName'] ?? "";
        $this->parentId = $result['parentId'] ?? 0;
        $this->areaLevel = $result['areaLevel'] ?? 0;
        $this->operateType = $result['operateType'] ?? 0;
    }

}