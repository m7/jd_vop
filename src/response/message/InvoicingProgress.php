<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 11 发票开票进度消息
 */
class InvoicingProgress
{
    /**
     * @var string  发票提报标识
     */
    public $markId;
    /**
     * @var int 状态
     * state状态：1：代表全部开票；2：代表驳回；3：部分开票；4：发票寄出
     */
    public $state;

    /**
     * 11.1 查询推送信息 Result 11 发票开票进度消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->markId = $result['markId'] ?? "";
        $this->state = $result['state'] ?? 0;
    }
}