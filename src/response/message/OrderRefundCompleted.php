<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息
 */
class OrderRefundCompleted
{

    public $customerId;
    /**
     * @var int|mixed
     */
    public $kaApplyId;
    /**
     * @var int|mixed
     */
    public $modifyDate;
    /**
     * @var int|mixed
     */
    public $orderId;
    /**
     * @var float|mixed
     */
    public $refundAmount;
    /**
     * @var array RefundDetail 退款明细：
     * OrderRefundCompletedRefundDetail
     */
    public $refundDetail;
    /**
     * @var array  RefundPaymentInfo 退款涉及到的⽀付信息
     * OrderRefundCompletedRefundPaymentInfos
     */
    public $refundPaymentInfos;
    /**
     * @var array RefundWaresInfo 退款涉及到的商品信息
     * OrderRefundCompletedRefundWaresInfos
     */
    public $refundWaresInfos;

    /**
     * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->customerId = $result['customerId'] ?? "";
        $this->kaApplyId = $result['kaApplyId'] ?? 0;
        $this->modifyDate = $result['modifyDate'] ?? 0;
        $this->orderId = $result['orderId'] ?? 0;
        $this->refundAmount = $result['refundAmount'] ?? 0;
        $this->refundDetail = [];
        foreach ($result['refundAmount'] ?? [] as $v) {
            $this->refundDetail[] = new OrderRefundCompletedRefundDetail($v);
        }
        foreach ($result['refundPaymentInfos'] ?? [] as $v) {
            $this->refundPaymentInfos[] = new OrderRefundCompletedRefundPaymentInfos($v);
        }
        foreach ($result['refundWaresInfos'] ?? [] as $v) {
            $this->refundWaresInfos[] = new OrderRefundCompletedRefundWaresInfos($v);
        }
    }
}