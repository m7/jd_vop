<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 105 订单维度售后完成
 */
class OrderAfterSalesCompletion
{
    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var array
     */
    public $batchIds;


    /**
     * 11.1 查询推送信息 Result 105 订单维度售后完成
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
        $this->batchIds = $result['batchIds'] ?? [];
        $this->pin = $result['pin'] ?? "";
    }
}