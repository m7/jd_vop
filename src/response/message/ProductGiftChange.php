<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 17 赠品促销变更消息
 */
class ProductGiftChange
{

    /**
     * @var int 商品编号
     */
    public $skuId;

    /**
     * 11.1 查询推送信息 Result 17 赠品促销变更消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->skuId = $result['skuId'] ?? 0;
    }
}