<?php

namespace jd_vop\response\message;

use jd_vop\response\Result;

/**
 * 11.1 查询推送信息 Result List
 */
class ResultList implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * 11.1 查询推送信息 Result List
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $data = [];
        foreach ($result as $v) {
            $data[] = Get::parse($v);
        }
        return new self($data);
    }
}