<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 2 商品价格变更
 * 此消息数据量大且频繁，为防止消息大量积压，开通需要走线下邮件开通申请。
 */
class PriceChange
{
    /**
     * @var int 商品编号
     */
    public $skuId;

    /**
     * 11.1 查询推送信息 Result 2 商品价格变更
     * 此消息数据量大且频繁，为防止消息大量积压，开通需要走线下邮件开通申请。
     * @param $result
     */
    public function __construct($result)
    {
        $this->skuId = $result['skuId'] ?? 0;
    }

}