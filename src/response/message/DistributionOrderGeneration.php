<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 12 配送单生成成功消息
 */
class DistributionOrderGeneration
{
    /**
     * @var int 京东订单编号
     */
    public $orderId;

    /**
     * 11.1 查询推送信息 Result 12 配送单生成成功消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
    }
}