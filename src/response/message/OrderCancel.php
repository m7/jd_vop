<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 10 订单取消消息
 * 不分取消原因
 */
class OrderCancel
{
    /**
     * @var int 京东订单编号
     */
    public $orderId;

    /**
     * 11.1 查询推送信息 Result 10 订单取消消息
     * 不分取消原因
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
    }
}