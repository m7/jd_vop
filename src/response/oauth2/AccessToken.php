<?php

namespace jd_vop\response\oauth2;


use jd_vop\response\Result;

/**
 * 2.3 获取Access Token Result
 */
class AccessToken implements Result
{
    /**
     * @var string 业务ID
     */
    public $uid;
    /**
     * @var string refresh_token的过期时间，毫秒级别,时间戳
     */
    public $refresh_token_expires;
    /**
     * @var int  当前时间，时间戳格式：1551663377887
     */
    public $time;
    /**
     * @var int access_token的有效期，单位：秒，有效期24小时
     */
    public $expires_in;
    /**
     * @var string 当access_token过期时，用于刷新access_token
     */
    public $refresh_token;
    /**
     * @var string 访问令牌，用于业务接口调用。有效期24小时
     */
    public $access_token;

    /**
     * 2.3 获取Access Token Result
     * @param $uid string 业务ID
     * @param $refresh_token_expires  string refresh_token的过期时间，毫秒级别,时间戳
     * @param $time  int  当前时间，时间戳格式：1551663377887
     * @param $expires_in int access_token的有效期，单位：秒，有效期24小时
     * @param $refresh_token string 当access_token过期时，用于刷新access_token
     * @param $access_token  string 访问令牌，用于业务接口调用。有效期24小时
     */
    public function __construct(string $uid, string $refresh_token_expires, int $time, int $expires_in, string $refresh_token, string $access_token)
    {
        $this->uid = $uid;
        $this->refresh_token_expires = $refresh_token_expires;
        $this->time = $time;
        $this->expires_in = $expires_in;
        $this->refresh_token = $refresh_token;
        $this->access_token = $access_token;
    }


    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $uid = $result['uid'] ?? 0;
        $refresh_token_expires = $result['refresh_token_expires'] ?? 0;
        $time = $result['time'] ?? 0;
        $expires_in = $result['expires_in'] ?? 0;
        $refresh_token = $result['refresh_token'] ?? "";
        $access_token = $result['access_token'] ?? "";
        return new self($uid, $refresh_token_expires, $time, $expires_in, $refresh_token, $access_token);
    }
}