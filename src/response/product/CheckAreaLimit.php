<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

class CheckAreaLimit implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * CheckAreaLimit constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return CheckAreaLimit
     */
    public static function parse($result): self
    {
        $data = json_decode($result, false);
        if ($data && (is_object($data)) || (is_array($data) && !empty(current($data)))) {
            $result = json_decode($result,1);
        }else{
            $result = null;
        }

        return new self($result);
    }

}