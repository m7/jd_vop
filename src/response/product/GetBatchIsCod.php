<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetBatchIsCod
 * @package jd_vop\response\product
 */
class GetBatchIsCod implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * GetBatchIsCod constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetBatchIsCod
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}