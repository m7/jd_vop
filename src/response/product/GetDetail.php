<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetDetail
 * @package jd_vop\response\product
 */
class GetDetail implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * GetDetail constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetDetail
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}