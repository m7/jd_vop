<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GoodsSearch
 * @package jd_vop\response\product
 */
class GoodsSearch implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * GoodsSearch constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GoodsSearch
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}