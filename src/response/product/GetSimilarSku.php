<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetSimilarSku
 * @package jd_vop\response\product
 */
class GetSimilarSku implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * GetSimilarSku constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetSimilarSku
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}