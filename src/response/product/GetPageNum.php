<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetPageNum
 * @package jd_vop\response\product
 */
class GetPageNum implements Result
{
    /**
     * @var object 返回值数据
     */
    public $data;


    /**
     * GetPageNum constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetPageNum
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}