<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class QuerySkuByPage
 * @package jd_vop\response\product
 */
class QuerySkuByPage implements Result
{
    /**
     * @var int 剩余页数
     */
    public $remainPage;
    /**
     * @var array skuIds集合
     */
    public $skuIds;
    /**
     * @var int 查询数据偏移量
     */
    public $offset;

    /**
     * @param $remainPage
     * @param $skuIds
     * @param $offset
     */
    public function __construct(int $remainPage, array $skuIds, int $offset)
    {
        $this->remainPage = $remainPage;
        $this->skuIds = $skuIds;
        $this->offset = $offset;
    }


    /**
     * @param $result
     * @return QuerySkuByPage
     */
    public static function parse($result): self
    {
        $remainPage = $result['remainPage'] ?? 0;
        $offset = $result['offset'] ?? 0;
        $skuIds = $result['skus'] ?? [];
        return new self($remainPage, $skuIds, $offset);
    }

}