<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class ProCheck
 * @package jd_vop\response\product
 */
class ProCheck implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * ProCheck constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return ProCheck
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}