<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetIsCod
 * @package jd_vop\response\product
 */
class GetIsCod implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * GetIsCod constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetIsCod
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}