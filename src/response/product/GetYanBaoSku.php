<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetYanBaoSku
 * @package jd_vop\response\product
 */
class GetYanBaoSku implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * GetYanBaoSku constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetYanBaoSku
     */
    public static function parse($result): self
    {

        return new self($result);
    }

}