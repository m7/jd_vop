<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class SkuImage
 * @package jd_vop\response\product
 */
class SkuImage implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * SkuImage constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return SkuImage
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}