<?php

namespace jd_vop\response;

interface Result
{
    public static function parse($result);
}