<?php

namespace jd_vop\response\order;

/**
 * 7.8 查询配送信息 Result WaybillCode
 */
class OrderTrackWaybillCode
{

    /**
     * @var int 订单号。
     */
    public $orderId;
    /**
     * @var int 父订单号。
     * 此字段为0 未拆单
     */
    public $parentId;
    /**
     * @var string 承运商。可以为“京东快递”或是京东配送站点名称或者商家自行录入的承运商名称。
     */
    public $carrier;
    /**
     * @var string  运单号。（可能没有，可能是京东订单号本身）
     */
    public $deliveryOrderId;

    /**
     * 7.8 查询配送信息 Result WaybillCode
     */
    public function __construct($data)
    {
        $this->orderId = $data['orderId'] ?? 0;
        $this->parentId = $data['parentId'] ?? 0;
        $this->carrier = $data['carrier'] ?? "";
        $this->deliveryOrderId = $data['deliveryOrderId'] ?? "";
    }

}