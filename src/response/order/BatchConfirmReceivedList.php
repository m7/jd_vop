<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.16 批量确认收货接口  Result 集合
 */
class BatchConfirmReceivedList implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * 7.16 批量确认收货接口  Result 集合
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $list = [];
        foreach ($result ?? [] as $v) {
            $list[] = new BatchConfirmReceived($v['jdOrderId'] ?? 0, $v['confirmState'] ?? "", $v['errorMsg'] ?? "");
        }
        return new self($list);
    }

}