<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;


/**
 * 7.7 查询订单详情-带子订单 子订单详情
 */
class SelectJdOrderChildOrder
{

    /**
     * @var int 父订单号。为0时，此订单为父单
     */
    public $pOrder;
    /**
     * @var int 订单状态。0为取消订单  1为有效。
     * TODO 修改为枚举
     */
    public $orderState;
    /**
     * @var int 京东订单编号。
     */
    public $jdOrderId;
    /**
     * @var int 物流状态。0 是新建  1是妥投   2是拒收。
     * TODO 修改为枚举
     */
    public $state;
    /**
     * @var int 预占确认状态。0没确认预占。   1已确认预占。
     * TODO 修改为枚举
     */
    public $submitState;
    /**
     * @var int 订单类型。1是父订单   2是子订单。
     * TODO 修改为枚举
     */
    public $type;
    /**
     * @var float 运费：收取运费时返回
     */
    public $freight;
    /**
     * @var array 商品列表
     */
    public $sku;
    /**
     * @var float 订单总金额（不含运费）
     */
    public $orderPrice;
    /**
     * @var float 订单未含税金额。
     * （备注：仅做页面展示，订单未税金额最终以发票票面为准。客户如需在下单后开票前，获取尽量精准的订单未税金额，
     * 如用作以未税金额扣减预算等场景，可以联系VOP侧产品配置白名单）
     */
    public $orderNakedPrice;
    /**
     * @var float 订单税额。
     */
    public $orderTaxPrice;

    /**
     * 7.7 查询订单详情 Result
     */
    public function __construct($data)
    {
        $this->pOrder = $data['pOrder'] ?? 0;
        $this->orderState = $data['orderState'] ?? 0;
        $this->jdOrderId = $data['jdOrderId'] ?? 0;
        $this->state = $data['state'] ?? 0;
        $this->submitState = $data['submitState'] ?? 0;
        $this->type = $data['type'] ?? 0;
        $this->freight = $data['freight'] ?? 0;
        $skus = [];
        foreach ($data['sku'] ?? [] as $item) {
            $skus[] = new SelectJdOrderSku($item);
        }
        $this->sku = $skus;
        $this->orderPrice = $data['orderPrice'] ?? 0;
        $this->orderNakedPrice = $data['orderNakedPrice'] ?? 0;
        $this->orderTaxPrice = $data['orderTaxPrice'] ?? 0;
    }


}