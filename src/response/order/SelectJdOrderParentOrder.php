<?php

namespace jd_vop\response\order;

/**
 * 7.7 查询订单详情-带子订单  父订单详情
 */
class SelectJdOrderParentOrder
{

    /**
     * @var int 京东订单编号。
     */
    public $jdOrderId;

    /**
     * @var array 商品列表
     */
    public $sku;
    /**
     * @var float 运费
     */
    public $freight;
    /**
     * @var float 订单总金额（不含运费）
     */
    public $orderPrice;
    /**
     * @var float 订单未含税金额。
     * （备注：仅做页面展示，订单未税金额最终以发票票面为准。客户如需在下单后开票前，获取尽量精准的订单未税金额，
     * 如用作以未税金额扣减预算等场景，可以联系VOP侧产品配置白名单）
     */
    public $orderNakedPrice;
    /**
     * @var float 订单税额。
     */
    public $orderTaxPrice;

    /**
     * 7.7 查询订单详情-带子订单  父订单详情
     * @param $data
     */
    public function __construct($data)
    {
        $this->jdOrderId = $data['jdOrderId']??0;
        $skus = [];
        foreach ($data['sku'] ?? [] as $item) {
            $skus[] = new SelectJdOrderSku($item);
        }
        $this->sku = $skus;
        $this->freight = $data['freight']??0;
        $this->orderPrice = $data['orderPrice']??0;
        $this->orderNakedPrice = $data['orderNakedPrice']??0;
        $this->orderTaxPrice = $data['orderTaxPrice']??0;
    }

}