<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.8 查询配送信息 Result
 */
class OrderTrack implements Result
{

    /**
     * @var  array OrderTrackOrderTrack 返回配送的信息
     */
    public $orderTrack;
    /**
     * @var array OrderTrackWaybillCode  返回订单的运单信息。
     * 当入参的waybillCode=1时，返回此字段
     */
    public $waybillCode;

    /**
     * @param array $orderTrack
     * @param array $waybillCode
     */
    public function __construct(array $orderTrack, array $waybillCode)
    {
        $this->orderTrack = $orderTrack;
        $this->waybillCode = $waybillCode;
    }

    /**
     * 7.8 查询配送信息 Result
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $orderTracks = [];
        foreach ($result['orderTrack'] ?? [] as $v) {
            $orderTracks[] = new OrderTrackOrderTrack($v);
        }
        $waybillCodes = [];
        foreach ($result['waybillCode'] ?? [] as $v) {
            $waybillCodes[] = new OrderTrackWaybillCode($v);
        }

        return new self($orderTracks, $waybillCodes);
    }

}