<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.12 查询新建订单列表 Result
 */
class CheckNewOrder implements Result
{

    /**
     * @var int 订单总数
     */
    public $total;
    /**
     * @var int  总页码数
     */
    public $totalPage;
    /**
     * @var int  当前页码
     */
    public $curPage;
    /**
     * @var array 订单信息列表
     */
    public $orders;

    /**
     * 7.12 查询新建订单列表 Result
     * @param int $total 订单总数
     * @param int $totalPage 总页码数
     * @param int $curPage 当前页码
     * @param array $orders 订单信息列表
     */
    public function __construct(int $total, int $totalPage, int $curPage, array $orders)
    {
        $this->total = $total;
        $this->totalPage = $totalPage;
        $this->curPage = $curPage;
        $this->orders = $orders;
    }


    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $total = $result['total'] ?? 0;
        $totalPage = $result['total'] ?? 0;
        $curPage = $result['total'] ?? 0;
        $orders = [];
        foreach ($result['orders'] ?? [] as $v) {
            $orders[] = CheckOrderOrder::parse($v);
        }
        return new self($total, $totalPage, $curPage, $orders);
    }
}