<?php

namespace jd_vop\response\order;


/**
 * 7.12 查询新建订单列表 Result order
 * 7.13 查询妥投订单列表 Result order
 * 7.14 查询拒收订单列表 Result order
 */
class CheckOrderOrder
{


    /**
     * @var int 京东订单编号。
     */
    public $jdOrderId;
    /**
     * @var int 物流状态。0 是新建  1是妥投   2是拒收。
     * TODO 修改为枚举
     */
    public $state;
    /**
     * @var int 是否挂起   0为为挂起    1为挂起
     * TODO 修改为枚举
     */
    public $hangUpState;
    /**
     * @var int 开票方式(1为随货开票，0为订单预借，2为集中开票 )
     * TODO 修改为枚举
     */
    public $invoiceState;
    /**
     * @var float 订单总金额（不含运费）
     */
    public $orderPrice;
    /**
     * @var string 订单创建时间。
     */
    public $time;


    /**
     * 7.12 查询新建订单列表 result order
     * @param int $jdOrderId 京东订单编号
     * @param int $state 物流状态。0 是新建  1是妥投   2是拒收。
     * @param int $hangUpState 是否挂起   0为为挂起    1为挂起
     * @param int $invoiceState 开票方式(1为随货开票，0为订单预借，2为集中开票 )
     * @param float $orderPrice 订单总金额
     * @param string $time
     */
    public function __construct(int    $jdOrderId, int $state,
                                int    $hangUpState, int $invoiceState, float $orderPrice,
                                string $time)
    {
        $this->jdOrderId = $jdOrderId;
        $this->state = $state;
        $this->hangUpState = $hangUpState;
        $this->invoiceState = $invoiceState;
        $this->orderPrice = $orderPrice;
        $this->time = $time;
    }


    public static function parse($result): self
    {

        $jdOrderId = $result['jdOrderId'] ?? 0;
        $state = $result['state'] ?? 0;
        $hangUpState = $result['hangUpState'] ?? 0;
        $invoiceState = $result['invoiceState'] ?? 0;
        $orderPrice = $result['orderPrice'] ?? 0;
        $time = $result['time'] ?? "";

        return new self($jdOrderId, $state,
            $hangUpState, $invoiceState, $orderPrice,
            $time);
    }
}