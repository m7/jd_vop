<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.5 确认预占库存订单 Result
 */
class ConfirmOrder implements Result
{

    /**
     * @var
     */
    public $data;


    /**
     * 7.5 确认预占库存订单 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}