<?php

namespace jd_vop\response\order;

/**
 * 7.7 查询订单详情 sku
 */
class SelectJdOrderSku
{

    /**
     * @var string 商品名称
     */
    public $name;
    /**
     * @var int 商品名称
     */
    public $skuId;
    /**
     * @var int 商品数量
     */
    public $num;
    /**
     * @var int 京东三级分类
     */
    public $category;
    /**
     * @var float 商品价格
     */
    public $price;
    /**
     * @var float 商品税率
     */
    public $tax;
    /**
     * @var int 主商品ID
     */
    public $oid;
    /**
     * @var int 商品类型 0 普通、1 附件、2 赠品、3延保
     * todo 修改枚举
     */
    public $type;
    /**
     * @var int 运费拆分价格
     */
    public $splitFreight;
    /**
     * @var int 商品税额
     */
    public $taxPrice;
    /**
     * @var int 商品未含税金额
     */
    public $nakedPrice;

    /**
     */
    public function __construct($v)
    {
        $this->name = $v['name'] ?? '';
        $this->skuId = $v['skuId'] ?? 0;
        $this->num = $v['num'] ?? 0;
        $this->category = $v['category'] ?? 0;
        $this->price = $v['price'] ?? 0;
        $this->tax = $v['tax'] ?? 0;
        $this->oid = $v['oid'] ?? 0;
        $this->type = $v['type'] ?? 0;
        $this->splitFreight = $v['splitFreight'] ?? 0;
        $this->taxPrice = $v['taxPrice'] ?? 0;
        $this->nakedPrice = $v['nakedPrice'] ?? 0;
    }


}