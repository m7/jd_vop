<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * Class UpdateCustomOrderExt
 * @package jd_vop\response\product
 */
class UpdateCustomOrderExt implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * UpdateCustomOrderExt constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return UpdateCustomOrderExt
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}