<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.1 查询运费 Result
 */
class GetFreight implements Result
{
    /**
     * @var  float  总运费
     */
    public $freight;
    /**
     * @var float 基础运费
     */
    public $baseFreight;
    /**
     * @var float 偏远地区加收运费
     */
    public $remoteRegionFreight;
    /**
     * @var string 需收取偏远运费的sku
     */
    public $remoteSku;
    /**
     * @var float 续重运费
     */
    public $conFreight;

    /**
     *  7.1 查询运费 Result
     * @param $freight  float  总运费
     * @param $baseFreight float 基础运费
     * @param $remoteRegionFreight float 偏远地区加收运费
     * @param $remoteSku string 需收取偏远运费的sku
     * @param $conFreight float 续重运费
     */
    public function __construct(float  $freight, float $baseFreight, float $remoteRegionFreight,
                                string $remoteSku, float $conFreight)
    {
        $this->freight = $freight;
        $this->baseFreight = $baseFreight;
        $this->remoteRegionFreight = $remoteRegionFreight;
        $this->remoteSku = $remoteSku;
        $this->conFreight = $conFreight;
    }


    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $freight = $result['freight'] ?: 0.0;
        $baseFreight = $result['baseFreight'] ?: 0.0;
        $remoteRegionFreight = $result['remoteRegionFreight'] ?: 0.0;
        $remoteSku = $result['remoteSku'] ?: "";
        $conFreight = $result['conFreight'] ?? 0.0;
        return new self($freight, $baseFreight, $remoteRegionFreight, $remoteSku, $conFreight);
    }
}