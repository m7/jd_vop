<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.16 查询配送预计送达时间 Result
 */
class GetPromiseTips implements Result
{

    /**
     * @var
     */
    public $data;


    /**
     * 7.16 查询配送预计送达时间 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }
}