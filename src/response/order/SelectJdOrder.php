<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;


/**
 * 7.7 查询订单详情 Result
 */
class SelectJdOrder implements Result
{

    /**
     * @var int 父订单号。为0时，此订单为父单
     */
    public $pOrder;
    /**
     * @var int 订单状态。0为取消订单  1为有效。
     * TODO 修改为枚举
     */
    public $orderState;
    /**
     * @var int 京东订单编号。
     */
    public $jdOrderId;
    /**
     * @var int 物流状态。0 是新建  1是妥投   2是拒收。
     * TODO 修改为枚举
     */
    public $state;
    /**
     * @var int 预占确认状态。0没确认预占。   1已确认预占。
     * TODO 修改为枚举
     */
    public $submitState;
    /**
     * @var int 订单类型。1是父订单   2是子订单。
     * TODO 修改为枚举
     */
    public $type;
    /**
     * @var float 运费：收取运费时返回
     */
    public $freight;
    /**
     * @var array 商品列表
     */
    public $sku;
    /**
     * @var float 订单总金额（不含运费）
     */
    public $orderPrice;
    /**
     * @var float 订单未含税金额。
     * （备注：仅做页面展示，订单未税金额最终以发票票面为准。客户如需在下单后开票前，获取尽量精准的订单未税金额，
     * 如用作以未税金额扣减预算等场景，可以联系VOP侧产品配置白名单）
     */
    public $orderNakedPrice;
    /**
     * @var float 订单税额。
     */
    public $orderTaxPrice;
    /**
     * @var int 订单类别。
     * 枚举值在constant\OrderType
     */
    public $orderType;
    /**
     * @var string 订单创建时间。
     */
    public $createOrderTime;
    /**
     * @var string 订单完成时间。
     */
    public $finishTime;
    /**
     * @var int 京东状态。
     * 枚举值在 constant\JdOrderState
     */
    public $jdOrderState;
    /**
     * @var int 支付方式。
     * 枚举值在 constant\PaymentType
     */
    public $paymentType;
    /**
     * @var array 混合支付明细。
     * 当paymentType为20混合支付，返回值
     * todo 修改返回对象
     */
    public $payDetails;

    /**
     * 7.7 查询订单详情 Result
     * @param int $pOrder 父订单号。为0时，此订单为父单
     * @param int $orderState 订单状态
     * @param int $jdOrderId 京东订单编号
     * @param int $state 物流状态。0 是新建  1是妥投   2是拒收。
     * @param int $submitState 预占确认状态
     * @param int $type 订单类型。1是父订单   2是子订单。
     * @param float $freight 运费：收取运费时返回
     * @param array $sku 商品列表
     * @param float $orderPrice 订单总金额
     * @param float $orderNakedPrice 订单未含税金额
     * @param float $orderTaxPrice 订单税额
     * @param int $orderType 订单类别
     * @param string $createOrderTime 订单创建时间
     * @param string $finishTime 订单完成时间
     * @param int $jdOrderState 京东状态
     * @param int $paymentType 支付方式
     * @param array $payDetails 混合支付明细。
     */
    public function __construct(int   $pOrder, int $orderState, int $jdOrderId, int $state,
                                int   $submitState, int $type, float $freight, array $sku,
                                float $orderPrice, float $orderNakedPrice, float $orderTaxPrice,
                                int   $orderType, string $createOrderTime, string $finishTime,
                                int   $jdOrderState, int $paymentType, array $payDetails)
    {
        $this->pOrder = $pOrder;
        $this->orderState = $orderState;
        $this->jdOrderId = $jdOrderId;
        $this->state = $state;
        $this->submitState = $submitState;
        $this->type = $type;
        $this->freight = $freight;
        $this->sku = $sku;
        $this->orderPrice = $orderPrice;
        $this->orderNakedPrice = $orderNakedPrice;
        $this->orderTaxPrice = $orderTaxPrice;
        $this->orderType = $orderType;
        $this->createOrderTime = $createOrderTime;
        $this->finishTime = $finishTime;
        $this->jdOrderState = $jdOrderState;
        $this->paymentType = $paymentType;
        $this->payDetails = $payDetails;
    }


    public static function parse($result)
    {
        $cOrder = $result['cOrder'] ?? 0;
        // 判断是否存在子订单
        if (is_array($cOrder)) {
            return SelectJdOrderHasChildOrder::parse($result);
        }
        $pOrder = $result['pOrder'] ?? 0;
        $orderState = $result['orderState'] ?? 0;
        $jdOrderId = $result['jdOrderId'] ?? 0;
        $state = $result['state'] ?? 0;
        $submitState = $result['submitState'] ?? 0;
        $type = $result['type'] ?? 0;
        $freight = $result['freight'] ?? 0;
        $skus = [];
        foreach ($result['sku'] ?? [] as $item) {
            $skus[] = new SelectJdOrderSku($item);
        }
        $orderPrice = $result['orderPrice'] ?? 0;
        $orderNakedPrice = $result['orderNakedPrice'] ?? 0;
        $orderTaxPrice = $result['orderTaxPrice'] ?? 0;
        $orderType = $result['orderType'] ?? 0;
        $createOrderTime = $result['createOrderTime'] ?? "";
        $finishTime = $result['finishTime'] ?? "";
        $jdOrderState = $result['jdOrderState'] ?? 0;
        $paymentType = $result['paymentType'] ?? 0;
        $payDetails = $result['payDetails'] ?? [];
        return new self($pOrder, $orderState, $jdOrderId, $state,
            $submitState, $type, $freight, $skus,
            $orderPrice, $orderNakedPrice, $orderTaxPrice,
            $orderType, $createOrderTime, $finishTime,
            $jdOrderState, $paymentType, $payDetails);
    }
}