<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;


/**
 * 7.7 查询订单详情-带子订单 Result
 */
class SelectJdOrderHasChildOrder
{

    /**
     * @var SelectJdOrderParentOrder 父订单详情
     */
    public $pOrder;
    /**
     * @var SelectJdOrderChildOrder 子订单详情
     */
    public $cOrder;
    /**
     * @var int 订单状态。0为取消订单  1为有效。
     * TODO 修改为枚举
     */
    public $orderState;
    /**
     * @var int 预占确认状态。0没确认预占。   1已确认预占。
     * TODO 修改为枚举
     */
    public $submitState;
    /**
     * @var int 订单类型。1是父订单   2是子订单。
     * TODO 修改为枚举
     */
    public $type;
    /**
     * @var int 订单类别。
     * 枚举值在constant\OrderType
     */
    public $orderType;
    /**
     * @var string 订单创建时间。
     */
    public $createOrderTime;
    /**
     * @var string 订单完成时间。
     */
    public $finishTime;
    /**
     * @var int 京东状态。
     * 枚举值在 constant\JdOrderState
     */
    public $jdOrderState;


    /**
     * 7.7 查询订单详情-带子订单
     * @param SelectJdOrderParentOrder $pOrder 父订单详情
     * @param SelectJdOrderChildOrder $cOrder 子订单详情
     * @param int $orderState 订单状态
     * @param int $submitState 预占确认状态
     * @param int $type 订单类型。1是父订单   2是子订单。
     * @param int $orderType 订单类别
     * @param string $createOrderTime 订单创建时间
     * @param string $finishTime 订单完成时间
     * @param int $jdOrderState 京东状态
     */
    public function __construct(SelectJdOrderParentOrder $pOrder, SelectJdOrderChildOrder $cOrder,
                                int                      $orderState, int $submitState, int $type,
                                int                      $orderType, string $createOrderTime, string $finishTime,
                                int                      $jdOrderState)
    {
        $this->pOrder = $pOrder;
        $this->cOrder = $cOrder;
        $this->orderState = $orderState;
        $this->submitState = $submitState;
        $this->type = $type;
        $this->orderType = $orderType;
        $this->createOrderTime = $createOrderTime;
        $this->finishTime = $finishTime;
        $this->jdOrderState = $jdOrderState;
    }


    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {

        $pOrder = new SelectJdOrderParentOrder($result['pOrder'] ?? []);
        $cOrder = new SelectJdOrderChildOrder($result['cOrder'] ?? []);
        $orderState = $result['orderState'] ?? 0;
        $submitState = $result['submitState'] ?? 0;
        $type = $result['type'] ?? 0;
        $orderType = $result['orderType'] ?? 0;
        $createOrderTime = $result['createOrderTime'] ?? "";
        $finishTime = $result['finishTime'] ?? "";
        $jdOrderState = $result['jdOrderState'] ?? 0;
        // 解析父子订单
        return new self($pOrder, $cOrder,
            $orderState, $submitState, $type,
            $orderType, $createOrderTime, $finishTime,
            $jdOrderState);
    }
}