<?php

namespace jd_vop\response\order;

/**
 * 7.8 查询配送信息 Result OrderTrack
 */
class OrderTrackOrderTrack
{
    /**
     * @var string 操作内容明细
     */
    public $content;
    /**
     * @var string 操作时间。日期格式为“yyyy-MM-dd hh:mm:ss”
     */
    public $msgTime;
    /**
     * @var string 操作员名称。
     */
    public $operator;

    /**
     * 7.8 查询配送信息 Result OrderTrack
     */
    public function __construct($data)
    {
        $this->content = $data['content'] ?? "";
        $this->msgTime = $data['msgTime'] ?? "";
        $this->operator = $data['operator'] ?? "";
    }
}