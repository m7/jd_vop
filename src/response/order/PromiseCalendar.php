<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * Class PromiseCalendar
 * @package jd_vop\response\product
 */
class PromiseCalendar implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * PromiseCalendar constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return PromiseCalendar
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}