<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.6 取消未确认订单 Result
 */
class Cancel implements Result
{

    /**
     * @var
     */
    public $data;


    /**
     * 7.6 取消未确认订单 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}