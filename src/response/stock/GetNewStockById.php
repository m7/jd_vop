<?php

namespace jd_vop\response\stock;

use jd_vop\response\Result;

/**
 * Class GetNewStockById
 * @package jd_vop\response\stock
 */
class GetNewStockById implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * GetNewStockById constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetNewStockById
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}