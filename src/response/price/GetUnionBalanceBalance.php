<?php

namespace jd_vop\response\price;

/**
 *  查询余额 Result Balance
 */
class GetUnionBalanceBalance
{

    /**
     * @var string 入参的pin值。
     */
    public $pin;
    /**
     * @var float 账户余额。
     */
    public $remainLimit;

    /**
     * 查询余额 Result Balance
     * @param string $pin 入参的pin值。
     * @param float $remainLimit 账户余额。
     */
    public function __construct(string $pin, float $remainLimit)
    {
        $this->pin = $pin;
        $this->remainLimit = $remainLimit;
    }
}