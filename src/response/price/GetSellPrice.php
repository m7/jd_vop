<?php

namespace jd_vop\response\price;

use jd_vop\response\Result;

/**
 * Class GetSellPrice
 * @package jd_vop\response\price
 */
class GetSellPrice implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * GetSellPrice constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetSellPrice
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}