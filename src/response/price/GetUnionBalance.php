<?php

namespace jd_vop\response\price;

use jd_vop\response\Result;

/**
 * 8.1 查询余额 Result
 */
class GetUnionBalance implements Result
{
    /**
     * @var GetUnionBalanceBalance 当type入参中包含1时，此对象出现。
     */
    public $balance;
    /**
     * @var GetUnionBalanceGeious 当type入参中包含2时，此对象出现。
     */
    public $geious;

    /**
     * @param mixed $balance
     * @param mixed $geious
     */
    public function __construct($balance, $geious)
    {
        $this->balance = $balance;
        $this->geious = $geious;
        if (empty($this->balance)) {
            unset($this->balance);
        }
        if (empty($this->geious)) {
            unset($this->geious);
        }
    }


    /**
     * @param $result
     * @return self
     */
    public static function parse($result): self
    {
        $balance = [];
        $geious = [];
        if (!empty($result['balance'] ?? [])) {
            $balance = new GetUnionBalanceBalance($result['balance']['pin'] ?? "",
                $result['balance']['remainLimit'] ?? 0);
        }
        if (!empty($result['geious'] ?? [])) {
            $geious = new GetUnionBalanceGeious($result['geious']['pin'] ?? "",
                $result['geious']['penaltySumAmt'] ?? 0,
                $result['geious']['creditLimit'] ?? 0,
                $result['geious']['debtSumAmt'] ?? 0,
                $result['geious']['remainLimit'] ?? 0);
        }
        return new self($balance, $geious);
    }
}