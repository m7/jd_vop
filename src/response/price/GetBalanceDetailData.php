<?php

namespace jd_vop\response\price;

use jd_vop\response\Result;

/**
 * 8.2 查询余额变动明细 Result Data
 */
class GetBalanceDetailData
{
    /**
     * @var int 余额明细ID
     */
    public $id;
    /**
     * @var int 账户类型  1：可用余额 2：锁定余额
     * todo 修改枚举
     */
    public $accountType;
    /**
     * @var float 金额（元），有正负，可以是零，表示订单流程变化，如退款时会先有一条退款申请的记录，金额为0
     */
    public $amount;
    /**
     * @var string  京东Pin
     */
    public $pin;
    /**
     * @var int 订单号
     */
    public $orderId;
    /**
     * @var int 业务类型
     */
    public $tradeType;
    /**
     * @var string 业务类型名称
     */
    public $tradeTypeName;
    /**
     * @var string 余额变动日期
     */
    public $createdDate;
    /**
     * @var string 备注信息
     */
    public $notePub;
    /**
     * @var int 业务号，一般由余额系统，在每一次操作成功后自动生成，也可以由前端业务系统传入
     */
    public $tradeNo;

    /**
     * 8.2 查询余额变动明细 Result Data
     * @param int $id 余额明细ID
     * @param int $accountType 账户类型  1：可用余额 2：锁定余额
     * todo 修改枚举
     * @param float $amount 金额（元），有正负，可以是零，表示订单流程变化，如退款时会先有一条退款申请的记录，金额为0
     * @param string $pin 京东Pin
     * @param int $orderId 订单号
     * @param int $tradeType 业务类型
     * @param string $tradeTypeName 业务类型名称
     * @param string $createdDate 余额变动日期
     * @param string $notePub 备注信息
     * @param int $tradeNo 业务号，一般由余额系统，在每一次操作成功后自动生成，也可以由前端业务系统传入
     */
    public function __construct(int $id, int $accountType, float $amount, string $pin, int $orderId, int $tradeType, string $tradeTypeName, string $createdDate, string $notePub, int $tradeNo)
    {
        $this->id = $id;
        $this->accountType = $accountType;
        $this->amount = $amount;
        $this->pin = $pin;
        $this->orderId = $orderId;
        $this->tradeType = $tradeType;
        $this->tradeTypeName = $tradeTypeName;
        $this->createdDate = $createdDate;
        $this->notePub = $notePub;
        $this->tradeNo = $tradeNo;
    }
}