<?php

namespace jd_vop\response\area;


use jd_vop\response\Result;

/**
 * 3.5 验证地址有效性 Result
 */
class CheckArea implements Result
{
    /**
     * @var bool 状态
     */
    public $success;
    /**
     * @var int 状态码
     */
    public $resultCode;
    /**
     * @var int 0
     */
    public $addressId;
    /**
     * @var string null
     */
    public $message;

    /**
     * 3.5 验证地址有效性 Result
     * @param $success bool 状态
     * @param $resultCode  int 状态码
     * @param $addressId int 0
     * @param $message  string null
     */
    public function __construct(bool $success, int $resultCode, int $addressId, string $message)
    {
        $this->success = $success;
        $this->resultCode = $resultCode;
        $this->addressId = $addressId;
        $this->message = $message;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        $success = $result['success'] ?? false;
        $resultCode = $result['resultCode'] ?? 0;
        $addressId = $result['addressId'] ?? 0;
        $message = $result['message'] ?? "";
        return new self($success, $resultCode, $addressId, $message);
    }

}