<?php

namespace jd_vop\response\area;
use jd_vop\response\Result;

/**
 * 3.6 地址详情转换京东地址编码 Result
 */
class GetJDAddressFromAddress implements Result
{
    /**
     * @var string 国家ID
     */
    public $nation_id;
    /**
     * @var string 国家名称
     */
    public $nation;
    /**
     * @var int 一级地址ID
     */
    public $province_id;
    /**
     * @var string 一级地址名称
     */
    public $province;
    /**
     * @var int 二级地址ID
     */
    public $city_id;
    /**
     * @var  string 二级地址名称
     */
    public $city;
    /**
     * @var  int 三级地址ID
     */
    public $county_id;
    /**
     * @var  string 三级地址名称
     */
    public $county;
    /**
     * @var int 四级地址ID
     */
    public $town_id;
    /**
     * @var string 四级地址名称
     */
    public $town;

    /**
     * 3.6 地址详情转换京东地址编码 Result
     * @param $nation_id string 国家ID
     * @param $nation string 国家名称
     * @param $province_id  int 一级地址ID
     * @param $province string 一级地址名称
     * @param $city_id  int 二级地址ID
     * @param $city  string 二级地址名称
     * @param $county_id int 三级地址ID
     * @param $county string 三级地址名称
     * @param $town_id int 四级地址ID
     * @param $town string 四级地址名称
     */
    public function __construct(string $nation_id, string $nation, int $province_id, string $province, int $city_id, string $city, int $county_id, string $county, int $town_id, string $town)
    {
        $this->nation_id = $nation_id;
        $this->nation = $nation;
        $this->province_id = $province_id;
        $this->province = $province;
        $this->city_id = $city_id;
        $this->city = $city;
        $this->county_id = $county_id;
        $this->county = $county;
        $this->town_id = $town_id;
        $this->town = $town;
    }


    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {

        $nation_id = $result['nationId'] ?? 0;
        $nation = $result['nation'] ?? "";
        $province_id = $result['provinceId'] ?? 0;
        $province = $result['province'] ?? "";
        $city_id = $result['cityId'] ?? 0;
        $city = $result['city'] ?? "";
        $county_id = $result['countyId'] ?? 0;
        $county = $result['county'] ?? "";
        $town_id = $result['townId'] ?? 0;
        $town = $result['town'] ?? "";
        return new self($nation_id, $nation, $province_id, $province, $city_id, $city, $county_id, $county, $town_id, $town);
    }

}