<?php

namespace jd_vop\response\area;

use jd_vop\response\Result;

/**
 * 3.4 查询四级地址 Result
 */
class Town implements Result
{

    /**
     * @var array 四级地址数据
     */
    public $towns;


    /**
     * 3.4 查询四级地址 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->towns = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }
}