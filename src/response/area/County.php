<?php

namespace jd_vop\response\area;
use jd_vop\response\Result;

/**
 * 3.3 查询三级地址 Result
 */
class County implements Result
{
    /**
     * @var array 三级地址列表
     */
    public $counties;


    /**
     * 3.3 查询三级地址 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->counties = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}