<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class UpdateSendInfo
 * @package jd_vop\response\aftersale
 */
class UpdateSendInfo implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * UpdateSendInfo constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return UpdateSendInfo
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}