<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class GetAfsOutline
 * @package jd_vop\response\aftersale
 */
class GetAfsOutline implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * GetAfsOutline constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetAfsOutline
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}