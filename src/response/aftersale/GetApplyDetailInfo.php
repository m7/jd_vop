<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class GetApplyDetailInfo
 * @package jd_vop\response\aftersale
 */
class GetApplyDetailInfo implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * GetApplyDetailInfo constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetApplyDetailInfo
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}