<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class GetGoodsAttributes
 * @package jd_vop\response\aftersale
 */
class GetGoodsAttributes implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * GetGoodsAttributes constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetGoodsAttributes
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}