<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class ConfirmAfsOrder
 * @package jd_vop\response\aftersale
 */
class ConfirmAfsOrder implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * ConfirmAfsOrder constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return ConfirmAfsOrder
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}