<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class CancelAfsApply
 * @package jd_vop\response\aftersale
 */
class CancelAfsApply implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * CancelAfsApply constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return CancelAfsApply
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}