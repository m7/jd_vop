<?php

namespace jd_vop\response\aftersale;

use jd_vop\response\Result;

/**
 * Class CreateAfsApply
 * @package jd_vop\response\aftersale
 */
class CreateAfsApply implements Result
{

    /**
     * @var
     */
    public $data;

    /**
     * CreateAfsApply constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return CreateAfsApply
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}