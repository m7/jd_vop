<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\response\area\CheckArea;
use jd_vop\response\area\City;
use jd_vop\response\area\County;
use jd_vop\response\area\GetJDAddressFromAddress;
use jd_vop\response\area\Province;
use jd_vop\response\area\Town;

class Area extends Jd_vop
{
    /**
     * 3.1 查询一级地址
     * @param $token  string access token
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function Province(string $token)
    {
        $request = new request\area\Province($token);
        $response = $this->Req($request, Province::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 3.2 查询二级地址
     * @param $token string access token
     * @param $province_id  int 一级地址ID
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function City(string $token, int $province_id)
    {
        $request = new request\area\City($token, $province_id);
        $response = $this->Req($request, City::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 3.3 查询三级地址
     * @param $token string access token
     * @param $city_id int 二级地址ID
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function County(string $token, int $city_id)
    {
        $request = new request\area\County($token, $city_id);
        $response = $this->Req($request, County::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 3.4 查询四级地址
     * @param $token string access token
     * @param $county_id  int 三级地址id
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function Town(string $token, int $county_id)
    {
        $request = new request\area\Town($token, $county_id);
        $response = $this->Req($request, Town::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 3.5 验证地址有效性
     * @param $token  string access token
     * @param $province_id int 一级地址ID
     * @param $city_id int 二级地址id
     * @param $county_id  int 三级地址id
     * @param $town_id  int 四级地址ID 若三级地址下无四级地址传0
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function CheckArea(string $token, int $province_id, int $city_id, int $county_id, int $town_id)
    {
        $request = new request\area\CheckArea($token, $province_id, $city_id, $county_id, $town_id);
        $response = $this->Req($request, CheckArea::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 3.6 地址详情转换京东地址编码
     * @param $token string access token
     * @param $address  string 详细地址
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function GetJDAddressFromAddress(string $token, string $address)
    {
        $request = new request\area\GetJDAddressFromAddress($token, $address);
        $response = $this->Req($request, GetJDAddressFromAddress::class);
        // 4. 成功数据
        return $response->result;
    }
}