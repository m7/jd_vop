<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\request\product\GetBatchIsCodQueryExt;
use jd_vop\request\product\GetDetailQueryExt;
use jd_vop\request\product\GetIsCodQueryExt;
use jd_vop\request\product\ProCheckQueryExt;
use jd_vop\response\product\GetDetail;
use jd_vop\response\product\GetPageNum;
use jd_vop\response\product\QuerySkuByPage;
use jd_vop\response\product\SkuImage;
use jd_vop\response\Response;
use phpDocumentor\Reflection\Types\String_;

/**
 * Class Product
 * @package jd_vop
 */
class Product extends Jd_vop
{

    /**
     * 4.1 查询商品池编号    分类
     * @param $token string 授权时获取的access token
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function GetPageNum($token)
    {
        $request = new request\product\GetPageNum($token);
        // 1. 网络问题
        $response = $this->Req($request, GetPageNum::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 4.2 查询池内商品编号
     * @param $token string 授权时获取的access token
     * @param $pageNum int 商品池编码（当前页数）
     * @param $pageSize int 当前页显示条数，默认20，最大1000
     * @param $offset int 偏移量
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function QuerySkuByPage($token, $pageNum, $pageSize, $offset)
    {
        $request = new request\product\QuerySkuByPage($token, $pageNum, $pageSize, $offset);
        // 1. 网络问题
        $response = $this->Req($request, QuerySkuByPage::class);
        // 4. 成功数据
        return $response->result;
    }
    /**
     * 4.3 查询商品详情
     * @param $token string 授权时获取的access token
     * @param $sku string 商品编号
     * @return mixed
     * @throws NetWorkException
     * @throws BizException
     * @throws DataException
     */
    public function GetDetail($token, $sku, GetDetailQueryExt $queryExts)
    {
        $request = new request\product\GetDetail($token, $sku, $queryExts);
        // 1. 网络问题
        $response = $this->Req($request, GetDetail::class);
        // 4. 成功数据
        return $response->result;
    }
    /**
     * 4.4 查询商品图片
     * @param $token string 授权时获取的access token
     * @param $sku string 商品编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function SkuImage($token, $sku){
        $request = new request\product\SkuImage($token, $sku);

        $response = $this->Req($request, SkuImage::class);

        return $response->result;

    }
    /**
     * 4.5 查询商品 上下架状态
     * @param $token string 授权时获取的access token
     * @param $sku string 商品编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function SkuState($token, $sku){
        $request = new request\product\SkuState($token, $sku);
        $response = $this->Req($request, \jd_vop\response\product\SkuState::class);
        return $response->result;
    }
    /**
     * 4.6 验证商品可售性
     * @param $token string 授权时获取的access token
     * @param $sku  string 商品编号
     * @param ProCheckQueryExt $queryExts string 扩展参数：英文逗号间隔输入
                                            noReasonToReturn 无理由退货类型
                                            thwa 无理由退货文案类型
                                            isSelf 是否自营
                                            isJDLogistics 是否京东配送
                                            taxInfo 商品税率
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function ProCheck($token, $sku, ProCheckQueryExt $queryExts){
        $request = new request\product\ProCheck($token, $sku, $queryExts );

        $response = $this->Req($request, \jd_vop\response\product\ProCheck::class);

        return $response->result;

    }
    /**
     * 4.7 查询商品区域购买限制
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function CheckAreaLimit($token, $skuIds, $province, $city, $county, $town){
        $request = new request\product\CheckAreaLimit($token, $skuIds, $province, $city, $county, $town);
        $response = $this->Req($request, \jd_vop\response\product\CheckAreaLimit::class);
        return $response->result;
    }
    /**
     * 4.8 查询赠品信息
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetSkuGift($token, $skuIds, $province, $city, $county, $town){
        $request = new request\product\GetSkuGift($token, $skuIds, $province, $city, $county, $town);
        $response = $this->Req($request, \jd_vop\response\product\GetSkuGift::class);
        return $response->result;
    }
    /**
     * 4.9 查询商品延保
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetYanBaoSku($token, $skuIds, $province, $city, $county, $town){
        $request = new request\product\GetYanBaoSku($token, $skuIds, $province, $city, $county, $town);
        $response = $this->Req($request, \jd_vop\response\product\GetYanBaoSku::class);
        return $response->result;
    }
    /**
     * 4.10 验证货到付款
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @param GetIsCodQueryExt $queryExts string skuIds 返回具体的skuId明细
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetIsCod($token, $skuIds, $province, $city, $county, $town , GetIsCodQueryExt $queryExts){

        $request = new request\product\GetIsCod($token, $skuIds, $province, $city, $county, $town ,$queryExts);
        $response = $this->Req($request, \jd_vop\response\product\GetIsCod::class);
        return $response->result;
    }
    /**
     * 4.11批量验证货到付款
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @param GetBatchIsCodQueryExt $queryExts string skuIds 返回具体的skuId明细
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetBatchIsCod($token, $skuIds, $province, $city, $county, $town , GetBatchIsCodQueryExt $queryExts){

        $request = new request\product\GetBatchIsCod($token, $skuIds, $province, $city, $county, $town ,$queryExts);
        $response = $this->Req($request, \jd_vop\response\product\GetBatchIsCod::class);
        return $response->result;
    }
    /**
     *
     * @param $token String 登录信息验证
     * @param $Keyword String 搜索关键词
     * @param $catId integer 分类ID ，三级分类ID
     * @param $pageIndex integer 当前页
     * @param $pageSize integer 当前页显示条数
     * @param $Min string 价格区间，最低价
     * @param $Max string 价格区间，最高价
     * @param $Brands string 品牌编码
     * @param $cid1 string 一级分类id
     * @param $cid2 string 二级分类id
     * @param $sortType string 排序 （例如：sale desc）
     * @param $priceCol string 价格汇总（例：'yes')
     * @param $extAttrCol string 扩展属性汇总（例：'yes'）
     * @param $mergeSku boolean 产品规格合并（true 、 false）
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GoodsSearch($token, $Keyword, $catId, $pageIndex, $pageSize, $Min, $Max, $Brands, $cid1, $cid2, $sortType, $priceCol, $extAttrCol, $mergeSku){
        $request = new request\product\GoodsSearch($token, $Keyword, $catId, $pageIndex, $pageSize, $Min, $Max, $Brands, $cid1, $cid2, $sortType, $priceCol, $extAttrCol, $mergeSku);
        $response = $this->Req($request, \jd_vop\response\product\GoodsSearch::class);
        return $response->result;
    }
    /**
     * 4.13 查询同类商品
     * @param $token String 登录信息验证
     * @param $skuIds string 商品编号
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetSimilarSku($token, $skuIds){
        $request = new request\product\GetSimilarSku($token, $skuIds);
        $response = $this->Req($request, \jd_vop\response\product\GetSimilarSku::class);
        return $response->result;
    }
    /**
     * 4.14 查询分类信息
     * @param $token  String 登录信息验证
     * @param $cid integer 分类ID
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetCategory($token, $cid){
        $request = new request\product\GetCategory($token, $cid);
        $response = $this->Req($request, \jd_vop\response\product\GetCategory::class);
        return $response->result;
    }
    /**
     * 4.15 商品可采校验接口
     * @param $token string 授权时获取的access token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级地址编号
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号
     * @param $skuIds string 商品编号，支持批量，以，分隔  (最高支持100个商品)
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function TotalCheckNew($token, $province, $city, $county, $town, $skuIds){
        $request = new request\product\TotalCheckNew($token, $province, $city, $county, $town, $skuIds);
        $response = $this->Req($request, \jd_vop\response\product\TotalCheckNew::class);
        return $response->result;
    }



}