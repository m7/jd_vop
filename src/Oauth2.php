<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\response\oauth2\AccessToken;

class Oauth2 extends Jd_vop
{
    /**
     * 2.3 获取Access Token
     * @throws DataException
     * @throws NetWorkException
     * @throws BizException
     */
    public function AccessToken()
    {
        $request = new request\oauth2\AccessToken($this->client_id, $this->username, $this->password_md5, $this->client_secret);
        $response = $this->Req($request, AccessToken::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 2.4 刷新 Access Token
     * @param $refresh_token  string 授权时获取的refresh_token
     * @return mixed|null
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function RefreshAccessToken(string $refresh_token)
    {
        $request = new request\oauth2\RefreshAccessToken($refresh_token, $this->client_id, $this->client_secret);
        $response = $this->Req($request, AccessToken::class);
        // 4. 成功数据
        return $response->result;
    }

}