<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.8查询退款明细 Request
 * Class GetOrderPayByOrderId
 * @package jd_vop\request\aftersale
 */
class GetOrderPayByOrderId extends Request
{

    /**
     * @var string 授权token
     */
    protected $token;

    /**
     * @var int required 订单号，子订单号
     */
    protected $orderId;

    /**
     * @var int Not required 售后服务单号
     */
    protected $refId;

    /**
     * @var int Not required 非必填，当前页码，起始页为1
     */
    protected $pageNo;

    /**
     * @var int not required 非必填，每页数据条数，不可大于100
     */
    protected $pageSize;

    /**
     * @var string
     */
    protected static $uri = "api/afterSale/getOrderPayByOrderId";


    /**
     * 9.8查询退款明细
     * GetOrderPayByOrderId constructor.
     * @param string $token     授权token
     * @param int $orderId      订单号，子订单号
     * @param int $refId        售后服务单号
     * @param int $pageNo       当前页码，起始页为1
     * @param int $pageSize     每页数据条数，不可大于100
     */
    public function __construct(string $token, int $orderId, $refId, $pageNo, $pageSize){
        parent::__construct();
        $this->token = $token;
        $this->orderId = $orderId;
        $this->refId = $refId;
        $this->pageNo = $pageNo;
        $this->pageSize = $pageSize;
    }


    /**
     * @param array $list
     * @return array
     */
    private function foreachUnset($list = []){
        foreach($list as $key=>$val){
            if(empty($val)) unset($list[$key]);
        }
        return $list;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'token' => $this->token,
            'orderId'=>$this->orderId,
            'refId'=>$this->refId,
            'pageNo'=>$this->pageNo,
            'pageSize'=>$this->pageSize,
        ];
        $param = $this->foreachUnset($param);

        return $param;
    }

}