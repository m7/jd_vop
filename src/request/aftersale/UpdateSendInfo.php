<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.4 填写运单信息 request
 */
class UpdateSendInfo extends Request
{
    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/updateSendInfo";

    /**
     * @var string access_token 授权时获取的access token
     */
    public $token;
    /**
     * @var string PIN。必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为申请售后的订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    public $customerPin;
    /**
     * @var int 订单号，即子单号
     */
    public $orderId;
    /**
     * @var string 第三方申请单号
     */
    public $thirdApplyId;
    /**
     * @var UpdateSendInfoWaybills 运单信息集合
     */
    public $waybillInfoList;


    /**
     * 9.4 填写运单信息 request
     * UpdateSendInfo constructor.
     * @param string $token 授权时获取的access token
     * @param $cunstomerPin PIN。必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为申请售后的订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，即子单号
     * @param string $thirdApplyId 第三方申请单号
     * @param UpdateSendInfoWaybills $waybillInfoList 运单信息集合
     */
    public function __construct(string $token , $customerPin, int $orderId, string $thirdApplyId , UpdateSendInfoWaybills $waybillInfoList)
    {
        parent::__construct();
        $this->token = $token;
        $this->customerPin = $customerPin;
        $this->orderId = $orderId;
        $this->thirdApplyId = $thirdApplyId;
        $this->waybillInfoList = $waybillInfoList;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'token' => $this->token,
            'customerPin' => $this->cunstomerPin,
            'orderId' => $this->orderId,
            'thirdApplyId' => $this->thirdApplyId,
            'waybillInfoList' => $this->waybillInfoList->parse(),
        ];
        if(empty($this->customerPin)) unset($param['customerPin']);

        $param = json_encode($param);

        return [
            'token' => $this->token,
            'param' => $param
        ];
    }


}