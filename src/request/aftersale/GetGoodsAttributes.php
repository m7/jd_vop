<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.1 查询商品售后属性
 * Class GetGoodsAttributes
 * @package jd_vop\request\aftersale
 */
class GetGoodsAttributes extends Request
{

    /**
     * @var string 授权token
     */
    protected $token;

    /**
     * @var int 订单号，子订单号
     */
    protected $orderId;

    /**
     * @var PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    protected $customerPin;

    /**
     * @var array 商品编码，多个用英文逗号隔开
     */
    protected $wareIds;

    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/getGoodsAttributes";

    /**
     * 9.1 查询商品售后属性
     * GetGoodsAttributes constructor.
     * @param string $token
     * @param int $orderId 订单号，子订单号
     * @param string $customerPin （Not required）PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param string $wareIds 商品编码
     */
    public function __construct(string $token, int $orderId, string $customerPin, $wareIds)
    {
        parent::__construct();
        $this->token = $token;
        $this->orderId = $orderId;
        $this->customerPin = $customerPin;
        $this->wareIds = $wareIds->parse();
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'orderId'=>$this->orderId,
            'customerPin'=>$this->customerPin,
            'wareIds'=>$this->wareIds
        ];
        if(empty($this->customerPin)) unset($param['customerPin']);

        $param = json_encode($param);
        return [
            'token' => $this->token,
            'param' => $param
        ];
    }

}