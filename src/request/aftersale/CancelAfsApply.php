<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.6取消售后申请 Request
 * Class CancelAfsApply
 * @package jd_vop\request\aftersale
 */
class CancelAfsApply extends Request
{

    /**
     * @var string 授权token
     */
    protected $token;

    /**
     * @var int 订单号，子订单号
     */
    protected $orderId;

    /**
     * @var string （Not required） PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    protected $customerPin;

    /**
     * @var string 第三方申请单号
     */
    protected $thirdApplyId;

    /**
     * @var string not required 备注“”可以默认写取消“”
     */
    protected $Remark;

    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/cancelAfsApply";

    /**
     * 9.6取消售后申请 Request
     * CancelAfsApply constructor.
     * @param string $token 授权token
     * @param string $customerPin  PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，子订单号
     * @param string $thirdApplyId 第三方申请单号
     * @param string $Remark 备注“”可以默认写取消“”
     */
    public function __construct(string $token, $customerPin, int $orderId, string $thirdApplyId, string $Remark){
        parent::__construct();
        $this->token = $token;
        $this->customerPin = $customerPin;
        $this->orderId = $orderId;
        $this->thirdApplyId = $thirdApplyId;
        $this->Remark = $Remark;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'customerPin'=>$this->customerPin,
            'orderId'=>$this->orderId,
            'thirdApplyId'=>$this->thirdApplyId,
            'Remark'=>$this->Remark,
        ];
        if(empty($this->customerPin)) unset($param['customerPin']);

        $param = json_encode($param);
        return [
            'token' => $this->token,
            'param' => $param
        ];
    }

}