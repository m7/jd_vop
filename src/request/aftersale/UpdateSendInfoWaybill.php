<?php

namespace jd_vop\request\aftersale;

/**
 * 9.4 填写运单信息
 */
/**
 * 9.4 填写运单信息
 * Class UpdateSendInfoWaybill
 * @package jd_vop\request\order
 */
class UpdateSendInfoWaybill
{
    /**
     * @var string 发货公司
     */
    public $expressCompany;
    /**
     * @var string 货运单号，字段长度不超过50
     */
    public $expressCode;
    /**
     * @var string 发货日期，格式为yyyy-MM-dd HH:mm:ss
     */
    public $deliverDate;
    /**
     * @var float 运费
     */
    public $freightMoney;
    /**
     * @var string 商品编号
     */
    public $wareId;
    /**
     * @var int 商品类型。10主商品，20赠品。
     */
    public $wareType;
    /**
     * @var int 商品数量
     */
    public $wareNum;

    /**
     * 9.4 填写运单信息
     * UpdateSendInfoWaybill constructor.
     * @param string $expressCompany 发货公司
     * @param string $expressCode 货运单号，字段长度不超过50
     * @param string $deliverDate 发货日期，格式为yyyy-MM-dd HH:mm:ss
     * @param float $freightMoney 运费
     * @param string $wareId 商品编号
     * @param int $wareType 商品类型。10主商品，20赠品。
     * @param int $wareNum 商品数量
     */
    public function __construct(string $expressCompany, string $expressCode, string $deliverDate, float  $freightMoney, string $wareId, int $wareType, int $wareNum)
    {
        $this->expressCompany = $expressCompany;
        $this->expressCode = $expressCode;
        $this->deliverDate = $deliverDate;
        $this->freightMoney = $freightMoney;
        $this->wareId = $wareId;
        $this->wareType = $wareType;
        $this->wareNum = $wareNum;
    }

    /**
     * @return string
     */
    public function getExpressCompany(): string
    {
        return $this->expressCompany;
    }

    /**
     * @param string $expressCompany 发货公司
     */
    public function setExpressCompany(string $expressCompany): void
    {
        $this->expressCompany = $expressCompany;
    }

    /**
     * @return string
     */
    public function getExpressCode(): string
    {
        return $this->expressCode;
    }

    /**
     * @param string $expressCode 货运单号，字段长度不超过50
     */
    public function setExpressCode(string $expressCode): void
    {
        $this->expressCode = $expressCode;
    }

    /**
     * @return string
     */
    public function getDeliverDate(): string
    {
        return $this->deliverDate;
    }

    /**
     * @param string $deliverDate 发货日期，格式为yyyy-MM-dd HH:mm:ss
     */
    public function setDeliverDate(string $deliverDate): void
    {
        $this->deliverDate = $deliverDate;
    }

    /**
     * @return float
     */
    public function getFreightMoney()
    {
        return $this->freightMoney;
    }

    /**
     * @param float $freightMoney 运费
     */
    public function setFreightMoney(float $freightMoney): void
    {
        $this->freightMoney = $freightMoney;
    }

    /**
     * @return string
     */
    public function getWareId(): string
    {
        return $this->wareId;
    }

    /**
     * @param string $wareId 商品编号
     */
    public function setWareId(string $wareId)
    {
        $this->wareId = $wareId;
        if ($this->wareId < 0) {
            $this->wareId = 0;
        }
    }

    /**
     * @return int
     */
    public function getWareType(): int
    {
        return $this->wareType;
    }

    /**
     * @param int $wareType 商品类型。10主商品，20赠品
     */
    public function setWareType(int $wareType)
    {
        $this->wareType = $wareType;
        if ($this->wareType < 0) {
            $this->wareType = 10;
        }
    }

    /**
     * @return int
     */
    public function getWareNum(): int
    {
        return $this->wareNum;
    }

    /**
     * @param int $wareNum 商品数量
     */
    public function setWareNum(int $wareNum)
    {
        $this->wareNum = $wareNum;
        if ($this->wareNum < 0) {
            $this->wareNum = 1;
        }
    }


}