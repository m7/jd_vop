<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.3 查询售后概要 Request
 * Class GetAfsOutline
 * @package jd_vop\request\aftersale
 */
class GetAfsOutline extends Request
{

    /**
     * @var string 授权toekn
     */
    protected $token;
    /**
     * @var  string PIN必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    protected $customerPin;
    /**
     * @var  string 订单号
     */
    protected $orderId;
    /**
     * @var string 第三方申请单号：thirdApplyId值为申请单编号时，表示特定申请单；thirdApplyId=1时表示选中所有申请单
     */
    protected $thirdApplyId;
    /**
     * @var int 第三方申请单号当前页号，默认为 1
     */
    protected $applyPageNo;
    /**
     * @var int 第三方申请每页行数，默认为20 ，max: 100
     */
    protected $applyPageSize;
    /**
     * @var string 商品编号。wareId为SKU编号时，表示特定SKU；wareId=1时表示选中所有SKU
     */
    protected $wareId;
    /**
     * @var int 商品维度信息当前页号，默认为 1
     */
    protected $warePageNo;
    /**
     * @var int 商品维度信息每页行数。默认为20 ，最大100
     */
    protected $warePageSize;
    /**
     * @var
     */
    protected $queryExts;

    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/getAfsOutline";

    /**
     * 9.3 查询售后概要 Request
     * GetAfsOutline constructor.
     * @param $token string 授权时获取的access token
     * @param $customerPin string PIN必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param $orderId string 订单号
     * @param $thirdApplyId string 第三方申请单号：thirdApplyId值为申请单编号时，表示特定申请单；thirdApplyId=1时表示选中所有申请单
     * @param $applyPageNo int 第三方申请单号当前页号，默认为 1
     * @param $applyPageSize int 第三方申请每页行数，默认为20 ，max: 100
     * @param $wareId string 商品编号。wareId为SKU编号时，表示特定SKU；wareId=1时表示选中所有SKU
     * @param $warePageNo int 商品维度信息当前页号，默认为 1
     * @param $warePageSize int 商品维度信息每页行数。默认为20 ，最大100
     * @param GetAfsOutlinQueryExt $queryExts
     */
    public function __construct(string $token, $customerPin, string $orderId, $thirdApplyId, $applyPageNo, $applyPageSize, $wareId, $warePageNo, $warePageSize, GetAfsOutlinQueryExt $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->customerPin = $customerPin;
        $this->orderId = $orderId;
        $this->thirdApplyId = $thirdApplyId;
        $this->applyPageNo = $applyPageNo;
        $this->applyPageSize = $applyPageSize;
        $this->warePageNo = $warePageNo;
        $this->warePageSize = $warePageSize;
        $this->queryExts = $queryExts;
    }


    /**
     * @param array $list
     * @return array
     */
    private function foreachUnset($list = []){
        foreach($list as $key=>$val){
            if(empty($val)) unset($list[$key]);
        }
        return $list;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'customerPin'=>$this->customerPin,
            'orderId'=>$this->orderId,
            'thirdApplyId'=>$this->thirdApplyId,
            'applyPageNo'=>$this->applyPageNo,
            'applyPageSize'=>$this->applyPageSize,
            'wareId'=>$this->wareId,
            'warePageNo'=>$this->warePageNo,
            'warePageSize'=>$this->warePageSize
        ];
        $param = $this->foreachUnset($param);
        $param = json_encode($param);
        return [
            'token' => $this->token,
            'param' => $param,
            'queryExts' => $this->queryExts->parse(),
        ];
    }

}