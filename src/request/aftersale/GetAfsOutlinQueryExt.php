<?php

namespace jd_vop\request\aftersale;

/**
 * Class GetAfsOutlinQueryExt
 * @package jd_vop\request\aftersale
 */
class GetAfsOutlinQueryExt
{
    /**
     * @var string 申请单是否可取消售后
     */
    public $canCancel;

    /**
     * @var string 申请单是否可以确认
     */
    public $canConfirm;

    /**
     * @var string 申请单是否可以填写发运信息
     */
    public $canSendSku;

    /**
     * 申请单是否可取消售后
     */
    public function setCanCancel()
    {
        $this->canCancel = 1;
    }
    /**
     * 申请单是否可以确认
     */
    public function setCanConfirm()
    {
        $this->canConfirm = 1;
    }
    /**
     * 申请单是否可以填写发运信息
     */
    public function setCanSendSku()
    {
        $this->canSendSku = 1;
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}