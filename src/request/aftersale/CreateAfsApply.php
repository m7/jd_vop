<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.2 申请售后
 * Class CreateAfsApply
 * @package jd_vop\request\aftersale
 */
class CreateAfsApply extends Request
{

    /**
     * @var string 授权token
     */
    protected $token;

    /**
     * @var int 订单号，子订单号
     */
    protected $orderId;

    /**
     * @var string （Not required） PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    protected $customerPin;

    /**
     * @var string 申请批次号，同一子订单下不可重复（长度最大20）
     */
    protected $thirdApplyId;

    /**
     * @var boolean not required 是否有发票
     */
    protected $isHasInvoice;

    /**
     * @var object AfsCustomerInfo 申请时用户信息
     */
    protected $customerInfo;
    /**
     * @var object AfsPickupWareInfo 申请时取件信息
     */
    protected $pickwareInfo;
    /**
     * @var object AfsReturnWareInfo 申请时返件信息
     */
    protected $returnWareInfo;
    /**
     * @var object list AfsApplyInfoItem 申请时申请条目,信息集合
     */
    protected $afsApplyInfoItemList;


    /**
     * @var string required 用户名
     */
    protected $customerName;
    /**
     * @var string required 联系人
     */
    protected $customerContactName;
    /**
     * @var string not required 联系电话，联系电话与手机号不能同时为空
     */
    protected $customerTel;
    /**
     * @var string not required 手机号，联系电话与手机号不能同时为空
     */
    protected $customerMobilePhone;
    /**
     * @var string not required Email邮箱
     */
    protected $customerEmail;
    /**
     * @var string not required 邮编
     */
    protected $customerPostcode;


    /**
     * @var int required 取件方式：4上门取件 7客户送货 40客户发货。
     */
    protected $pickwareType;
    /**
     * @var int required 取件省
     */
    protected $pickWareProvince;
    /**
     * @var int required 取件市
     */
    protected $pickWareCity;
    /**
     * @var int required 取件县
     */
    protected $pickWareCounty;
    /**
     * @var int required 取件乡镇
     */
    protected $pickWareVillage;
    /**
     * @var string required 取件街道地址
     */
    protected $pickWareAddress;
    /**
     * @var string not required 预约取件开始时间。开始时间不可早于当前时间+2小时。格式：2014-09-23 09:00:00
     */
    protected $reserveDateBegin;
    /**
     * @var string not required 预约取件结束时间。格式：2014-09-23 19:00:00
     */
    protected $reserveDateEnd;


    /**
     * @var int required 返件方式。10自营配送（与4取件固定搭配），20第三方配送（与40客户发货固定搭配）
     */
    protected $returnWareType;
    /**
     * @var int required 返件省
     */
    protected $returnWareProvince;
    /**
     * @var int required 返件市
     */
    protected $returnWareCity;
    /**
     * @var int required 返件县
     */
    protected $returnWareCountry;
    /**
     * @var int required 返件乡镇
     */
    protected $returnWareVillage;
    /**
     * @var int required 返件街道地址
     */
    protected $returnWareAddress;


    /**
     * @var int required 客户期望售后类型。10退货，20换货，30维修
     */
    protected $customerExpect;
    /**
     * @var object WareDescInfo not required 商品描述信息
     */
    protected $wareDescInfo;
    /**
     * @var object required WareDetailInfo 商品明细
     */
    protected $wareDetailInfo;


    /**
     * @var Boolean not required 是否需要检测报告
     */
    protected $isNeedDetectionReport;

    /**
     * @var Boolean not required 是否有防损吊牌
     */
    protected $lossPreventionTagFlag;
    /**
     * @var boolean not required 是否有包装
     */
    protected $isHasPackage;
    /**
     * @var int not required 包装描述
     */
    protected $packageDesc;
    /**
     * @var string not required 问题描述文字
     */
    protected $questionDesc;
    /**
     * @var string not required 问题描述图片链接。多个图片以“,”分隔
     */
    protected $questionPic;

    /**
     * @var string required 商品编号
     */
    protected $wareId;
    /**
     * @var string required 主商品编号
     */
    protected $mainWareId;
    /**
     * @var string required 商品名称
     */
    protected $wareName;
    /**
     * @var int required 商品申请数量
     */
    protected $wareNum;
    /**
     * @var string not required 附件描述
     */
    protected $wareDescribe;
    /**
     * @var decimal not required 支付金额：商品单价*数量
     */
    protected $payPrice;
    /**
     * @var int required 商品类型：10主商品，20赠品
     */
    protected $wareType;

    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/createAfsApply";

    /**
     * 9.2 申请售后 Request
     * CreateAfsApply constructor.
     * @param string $token                 授权token
     * @param int $orderId                  子订单号
     * @param string $customerPin           PIN。必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param string $thirdApplyId          申请批次号，同一子订单下不可重复（长度最大20）
     * @param boolean $isHasInvoice         是否有发票
     * @param string $customerName          用户名
     * @param string $customerContactName   联系人
     * @param string $customerTel           联系电话，联系电话与手机号不能同时为空
     * @param string $customerMobilePhone   手机号，联系电话与手机号不能同时为空
     * @param string $customerEmail         Email
     * @param string $customerPostcode      邮编
     * @param int $pickwareType             取件方式。4上门取件7客户送货， 40客户发货。
     * @param int $pickWareProvince         取件省
     * @param int $pickWareCity             取件市
     * @param int $pickWareCounty           取件县
     * @param int $pickWareVillage          取件乡镇
     * @param string $pickWareAddress       取件街道地址
     * @param string $reserveDateBegin      预约取件开始时间。开始时间不可早于当前时间+2小时。格式：2014-09-23 09:00:00
     * @param string $reserveDateEnd        预约取件结束时间。格式：2014-09-23 19:00:00
     * @param int $returnWareType           返件方式。10自营配送（与4取件固定搭配），20第三方配送（与40客户发货固定搭配）
     * @param int $returnWareProvince       返件省
     * @param int $returnWareCity           返件市
     * @param int $returnWareCountry        返件县
     * @param int $returnWareVillage        返件乡镇
     * @param string $returnWareAddress     返件街道地址
     * @param int $customerExpect           客户期望售后类型。10退货，20换货，30维修
     * @param Bool $isNeedDetectionReport   是否需要检测报告
     * @param bool $lossPreventionTagFlag   是否有防损吊牌
     * @param bool $isHasPackage            是否有包装
     * @param int $packageDesc              包装描述
     * @param string $questionDesc          问题描述文字
     * @param string $questionPic           问题描述图片链接。多个图片以“,”分隔
     * @param string $wareId                商品编号
     * @param string $mainWareId            主商品编号
     * @param string $wareName              商品名称
     * @param int $wareNum                  商品申请数量
     * @param string $wareDescribe          附件描述
     * @param decimal $payPrice             支付金额，即“商品单价*数量”
     * @param int $wareType                 商品类型。10主商品，20赠品
     */
    public function __construct(string $token, int $orderId, $customerPin, string $thirdApplyId, $isHasInvoice, string $customerName, string $customerContactName, $customerTel, $customerMobilePhone, $customerEmail,
                                $customerPostcode, int $pickwareType, int $pickWareProvince, int $pickWareCity, int $pickWareCounty, int $pickWareVillage , string $pickWareAddress,
                                $reserveDateBegin, $reserveDateEnd, int $returnWareType, int $returnWareProvince, int $returnWareCity, int $returnWareCountry, int $returnWareVillage,
                                string $returnWareAddress, int $customerExpect, $isNeedDetectionReport, $lossPreventionTagFlag, $isHasPackage, $packageDesc, $questionDesc,
                                $questionPic, string $wareId, string $mainWareId, string $wareName, int $wareNum, $wareDescribe, $payPrice, int $wareType){
        parent::__construct();
        $this->token = $token;
        $this->orderId = $orderId;
        $this->customerPin = $customerPin;
        $this->thirdApplyId = $thirdApplyId;
        $this->isHasInvoice = $isHasInvoice;
        $this->customerName = $customerName;
        $this->customerContactName = $customerContactName;
        $this->customerTel = $customerTel;
        $this->customerMobilePhone = $customerMobilePhone;
        $this->customerEmail = $customerEmail;
        $this->customerPostcode = $customerPostcode;
        $this->pickwareType = $pickwareType;
        $this->pickWareProvince = $pickWareProvince;
        $this->pickWareCity = $pickWareCity;
        $this->pickWareCounty = $pickWareCounty;
        $this->pickWareVillage = $pickWareVillage;
        $this->pickWareAddress = $pickWareAddress;
        $this->reserveDateBegin = $reserveDateBegin;
        $this->reserveDateEnd = $reserveDateEnd;
        $this->returnWareType = $returnWareType;
        $this->returnWareProvince = $returnWareProvince;
        $this->returnWareCity = $returnWareCity;
        $this->returnWareCountry = $returnWareCountry;
        $this->returnWareVillage = $returnWareVillage;
        $this->returnWareAddress = $returnWareAddress;
        $this->customerExpect = $customerExpect;
        $this->isNeedDetectionReport = $isNeedDetectionReport;
        $this->lossPreventionTagFlag = $lossPreventionTagFlag;
        $this->isHasPackage = $isHasPackage;
        $this->packageDesc = $packageDesc;
        $this->questionDesc = $questionDesc;
        $this->questionPic = $questionPic;
        $this->wareId = $wareId;
        $this->mainWareId = $mainWareId;
        $this->wareName = $wareName;
        $this->wareNum = $wareNum;
        $this->wareDescribe = $wareDescribe;
        $this->payPrice = $payPrice;
        $this->wareType = $wareType;

        $this->customerInfo = $this->setCustomerInfo();
        $this->pickwareInfo = $this->setPickwareInfo();
        $this->returnWareInfo = $this->setReturnWareInfo();
        $this->afsApplyInfoItemList = $this->setAfsApplyInfoItemList();
    }

    /**
     * @param array $list
     * @return array
     */
    private function foreachUnset($list = []){
        foreach($list as $key=>$val){
            if(empty($val)) unset($list[$key]);
        }
        return $list;
    }

    /**
     * @return array
     */
    private function setCustomerInfo(){
        $AfsCustomerInfo = [
            'customerName'=>$this->customerName,
            'customerContactName'=>$this->customerContactName,
            'customerTel'=>$this->customerTel,
            'customerMobilePhone'=>$this->customerMobilePhone,
            'customerEmail'=>$this->customerEmail,
            'customerPostcode'=>$this->customerPostcode,
        ];
        return $this->foreachUnset($AfsCustomerInfo);
    }

    /**
     * @return array
     */
    private function setPickwareInfo(){
        $AfsPickupWareInfo = [
            'pickwareType'=>$this->pickwareType,
            'pickWareProvince'=>$this->pickWareProvince,
            'pickWareCity'=>$this->pickWareCity,
            'pickWareCounty'=>$this->pickWareCounty,
            'pickWareVillage'=>$this->pickWareVillage,
            'pickWareAddress'=>$this->pickWareAddress,
            'reserveDateBegin'=>$this->reserveDateBegin,
            'reserveDateEnd'=>$this->reserveDateEnd,
        ];
        return $this->foreachUnset($AfsPickupWareInfo);
    }

    /**
     * @return array
     */
    private function setReturnWareInfo(){
        $AfsReturnWareInfo = [
            'returnWareType'=>$this->returnWareType,
            'returnWareProvince'=>$this->returnWareProvince,
            'returnWareCity'=>$this->returnWareCity,
            'returnWareCountry'=>$this->returnWareCountry,
            'returnWareVillage'=>$this->returnWareVillage,
            'returnWareAddress'=>$this->returnWareAddress,
        ];
        return $AfsReturnWareInfo;
    }

    /**
     * @return array
     */
    private function setAfsApplyInfoItemList(){
        $this->wareDescInfo = $this->setWareDescInfo();
        $this->wareDetailInfo = $this->setWareDetailInfo();
        $AfsApplyInfoItem = [
            'customerExpect'=>$this->customerExpect,
            'wareDescInfo'=>$this->wareDescInfo,
            'wareDetailInfo'=>$this->wareDetailInfo,
        ];
        return $this->foreachUnset($AfsApplyInfoItem);
    }

    /**
     * @return array
     */
    private function setWareDescInfo(){
        $WareDescInfo = [
            'isNeedDetectionReport'=>$this->isNeedDetectionReport,
            'lossPreventionTagFlag'=>$this->lossPreventionTagFlag,
            'isHasPackage'=>$this->isHasPackage,
            'packageDesc'=>$this->packageDesc,
            'questionDesc'=>$this->questionDesc,
            'questionPic'=>$this->questionPic,
        ];
        return $this->foreachUnset($WareDescInfo);
    }

    /**
     * @return array
     */
    private function setWareDetailInfo(){
        $WareDetailInfo = [
            'wareId'=>$this->wareId,
            'mainWareId'=>$this->mainWareId,
            'wareName'=>$this->wareName,
            'wareNum'=>$this->wareNum,
            'wareDescribe'=>$this->wareDescribe,
            'payPrice'=>$this->payPrice,
            'wareType'=>$this->wareType,
        ];
        return $this->foreachUnset($WareDetailInfo);
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $param = [
            'customerPin'=>$this->customerPin,
            'orderId'=>$this->orderId,
            'thirdApplyId'=>$this->thirdApplyId,
            'isHasInvoice'=>$this->isHasInvoice,
            'customerInfo'=>$this->customerInfo,
            'pickwareInfo'=>$this->pickwareInfo,
            'returnWareInfo'=>$this->returnWareInfo,
            'afsApplyInfoItemList'=>$this->afsApplyInfoItemList
        ];
        $param = $this->foreachUnset($param);

        $param = json_encode($param);
        return [
            'token' => $this->token,
            'param' => $param
        ];
    }

}