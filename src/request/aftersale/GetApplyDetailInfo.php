<?php

namespace jd_vop\request\aftersale;

use jd_vop\request\Request;

/**
 * 9.7 查询售后申请单明细 Request
 * Class GetApplyDetailInfo
 * @package jd_vop\request\aftersale
 */
class GetApplyDetailInfo extends Request
{

    /**
     * @var string 授权token
     */
    protected $token;

    /**
     * @var int 订单号，子订单号
     */
    protected $orderId;

    /**
     * @var string （Not required） PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     */
    protected $customerPin;

    /**
     * @var string 第三方申请单号
     */
    protected $thirdApplyId;

    /**
     * @var string required 获取信息模块：1、代表增加获取退款明细、2、代表增加获取客户发货信息；多个用英文逗号隔开（举例：1,2）
     */
    protected $appendInfoSteps ;

    /**
     * @var string
     */
    protected static $uri = "api/afterSaleNew/getApplyDetailInfo";

    /**
     * 9.7 查询售后申请单明细 Request
     * CancelAfsApply constructor.
     * @param string $token 授权token
     * @param string $customerPin  PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，子订单号
     * @param string $thirdApplyId 第三方申请单号
     * @param string $appendInfoSteps 获取信息模块：1、代表增加获取退款明细、2、代表增加获取客户发货信息；多个用英文逗号隔开（举例：1,2）
     */
    public function __construct(string $token ,$customerPin ,int $orderId ,string $thirdApplyId ,string $appendInfoSteps){
        parent::__construct();
        $this->token = $token;
        $this->customerPin = $customerPin;
        $this->orderId = $orderId;
        $this->thirdApplyId = $thirdApplyId;
        $this->appendInfoSteps = $appendInfoSteps;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        if(empty($this->appendInfoSteps) || !in_array($this->appendInfoSteps,[1,2])){
            $this->appendInfoSteps = [1];
        }else{
            $this->appendInfoSteps = explode(',',$this->appendInfoSteps);
        }

        $param = [
            'customerPin'=>$this->customerPin,
            'orderId'=>$this->orderId,
            'thirdApplyId'=>$this->thirdApplyId,
            'appendInfoSteps'=>$this->appendInfoSteps,
        ];
        if(empty($this->customerPin)) unset($param['customerPin']);

        $param = json_encode($param);
        return [
            'token' => $this->token,
            'param' => $param
        ];
    }

}