<?php

namespace jd_vop\request\price;

class GetSellPriceQueryExt
{
    /**
     * @var 大客户默认价格(根据合同类型查询价格)
     */
    public $price;

    /**
     * @var 市场价
     */
    public $marketPrice;
    /**
     * @var 税率
     */
    public $containsTax;
    /**
     * @var 未税价
     */
    public $nakedPrice;

    public function setPrice()
    {
        $this->price = 1;
    }

    public function setMarketPrice()
    {
        $this->marketPrice = 1;
    }

    public function setContainsTax()
    {
        $this->containsTax = 1;
    }

    public function setNakedPrice()
    {
        $this->nakedPrice = 1;
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}