<?php

namespace jd_vop\request\price;

use jd_vop\request\Request;

/**
 * 8.1 查询余额 Request
 */
class GetUnionBalance extends Request
{

    protected static $uri = "api/price/getUnionBalance";

    /**
     * @var string 授权时获取的access token
     */
    public $token;
    /**
     * @var string 京东PIN。必须是相同合同下的pin。
     */
    public $pin;
    /**
     *
     * @var string 账户余额类型。
     * 多选，可用英文逗号拼接。
     * 参考枚举值如下： todo 修改枚举
     * 1：账户余额。
     * 2：金采余额。
     */
    public $type;

    /**
     * 8.1 查询余额 Request
     * @param string $token 授权时获取的access token
     * @param string $pin 京东PIN。必须是相同合同下的pin。
     * @param string $type 账户余额类型。
     * 多选，可用英文逗号拼接。
     * 参考枚举值如下： todo 修改枚举
     * 1：账户余额。
     * 2：金采余额。
     */
    public function __construct(string $token, string $pin, string $type)
    {
        parent::__construct();
        $this->token = $token;
        $this->pin = $pin;
        $this->type = $type;
    }


    public function params(): array
    {
        return [
            'token' => $this->token,
            'pin' => $this->pin,
            'type' => $this->type,
        ];
    }
}