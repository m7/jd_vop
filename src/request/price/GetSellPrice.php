<?php

namespace jd_vop\request\price;

use jd_vop\request\Request;

/**
 * Class GetSellPrice
 * @package jd_vop\request\product
 */
class GetSellPrice extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 商品编码，请以，(英文逗号)分割。例如：129408,129409 (最高支持100个商品)
     */
    protected $skuIds;
    /**
     * @var array [{"skuId":1234,"num":1},{"skuId":1234,"num":1}]，不传时取sku对应的商品编号，数量默认为1。此字段有值则不校验sku字段
     */
    protected $skuInfos;
    /**
     * @var GetSellPriceQueryExt
     */
    protected $queryExts;
    /**
     * @var string
     */
    protected static $uri = "api/price/getSellPrice";


    /**
     * 5.1 查询商品售卖价 Request
     * GetSellPrice constructor.
     * @param $token string 授权token
     * @param $skuIds string 商品编码，请以，(英文逗号)分割。例如：129408,129409 (最高支持100个商品)
     * @param $skuInfos
     * @param GetSellPriceQueryExt $queryExts
     */
    public function __construct($token, $skuIds, $skuInfos, GetSellPriceQueryExt $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->skuIds = $skuIds;
        $this->skuInfos = $skuInfos;
        $this->queryExts = $queryExts;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'sku' => $this->skuIds,
            'skuInfos' => $this->skuInfos,
            'queryExts' => $this->queryExts->parse(),
        ];
    }

}