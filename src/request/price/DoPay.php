<?php

namespace jd_vop\request\price;

use jd_vop\request\Request;

/**
 * 8.3  重新支付接口 Request
 */
class DoPay extends Request
{

    protected static $uri = "api/order/doPay";

    /**
     * @var string 授权时获取的access token
     */
    public $token;
    /**
     * @var int $jdOrderId 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;

    /**
     * 8.3  重新支付接口 Request
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     */
    public function __construct(string $token, int $jdOrderId)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
    }


    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
        ];
    }
}