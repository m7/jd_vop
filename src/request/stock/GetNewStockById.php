<?php

namespace jd_vop\request\stock;

use jd_vop\request\Request;

/**
 * Class GetNewStockById
 * @package jd_vop\request\stock
 */
class GetNewStockById extends Request
{
    /**
     * @var string 授权时获取的access token
     */
    protected $token;
    /**
     * @var string 商品和数量  [{skuId: 569172,num:101}] （ “{skuId: 569172,num:10}”为1条记录，此参数最多传入100条记录 ）
     */
    protected $skuNums;
    /**
     * @var string 格式：13_1000_4277_0 (分别代表1、2、3、4级地址)
     */
    protected $area;
    /**
     * @var string
     */
    protected static $uri = "api/stock/getNewStockById";

    /**
     * 6.1 查询商品库存 Request
     * GetNewStockById constructor.
     * @param $token string 授权时获取的access token
     * @param $skuNums string 商品和数量  [{skuId: 569172,num:101}] （ “{skuId: 569172,num:10}”为1条记录，此参数最多传入100条记录 ）
     * @param $area string 格式：13_1000_4277_0 (分别代表1、2、3、4级地址)
     */
    public function __construct($token , $skuNums , $area)
    {
        parent::__construct();
        $this->token = $token;
        $this->skuNums = $skuNums;
        $this->area = $area;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'skuNums' => $this->skuNums,
            'area' => $this->area
        ];
    }

}