<?php

namespace jd_vop\request\message;

use jd_vop\request\Request;

class Del extends Request
{

    protected static $uri = "api/message/del";

    public $token;
    public $id;

    /**
     * @param $token
     * @param $id
     */
    public function __construct($token, $id)
    {
        parent::__construct();
        $this->token = $token;
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return ['token' => $this->token, 'id' => $this->id];
    }

}