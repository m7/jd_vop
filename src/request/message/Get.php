<?php

namespace jd_vop\request\message;

use jd_vop\request\Request;

/**
 *  11.1 查询推送信息
 */
class Get extends Request
{

    protected static $uri = "api/message/get";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var GetType  推送类型。支持多个组合，英文逗号间隔。例如1,2,3。
     * GetType->parse()
     */
    public $type;

    /**
     * @param string $token access token
     * @param GetType $type 推送类型。支持多个组合，英文逗号间隔。例如1,2,3。
     */
    public function __construct(string $token, GetType $type)
    {
        parent::__construct();
        $this->token = $token;
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'type' => $this->type->parse()
        ];
    }


}