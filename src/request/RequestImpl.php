<?php

namespace jd_vop\request;

use Psr\Http\Message\ResponseInterface;

interface RequestImpl
{

    public function params(): array;
    public function doRequest(): ResponseInterface;
}