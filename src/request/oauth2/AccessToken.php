<?php

namespace jd_vop\request\oauth2;

use jd_vop\request\Request;

/**
 * 2.3 获取Access Token Request
 */
class AccessToken extends Request
{
    /**
     * @var string
     */
    public static $uri = "oauth2/accessToken";

    /**
     * @var string 即对接账号(由京东人员提供)
     */
    protected $client_id;
    /**
     * @var string 京东用户名（需要保持与运营提供的采购账号一致，需要注意区分大小写）
     */
    protected $username;
    /**
     * @var string 京东的密码，该字段需要把京东提供的密码进行32位md5加密，然后将结果转成小写进行传输。
     */
    protected $password_md5;
    /**
     * @var string client_secret的值是京东分配的，以邮件形式发送给客户。
     */
    protected $client_secret;
    /**
     * 当前时间，格式为“yyyy-MM-dd hh:mm:ss”
     *与京东服务器时差不能相差半小时以上，京东服务器时间为北京时间（年月日和时分秒中间有空格）
     * @var false|string
     */
    protected $timestamp;
    /**
     * @var string 该值固定为access_token
     */
    protected $grant_type;

    /**
     * 2.3 获取Access Token Request
     * @param $client_id  string 即对接账号(由京东人员提供)
     * @param $username string 京东用户名（需要保持与运营提供的采购账号一致，需要注意区分大小写）
     * @param $password_md5 string 京东的密码，该字段需要把京东提供的密码进行32位md5加密，然后将结果转成小写进行传输。
     * @param $client_secret string client_secret的值是京东分配的，以邮件形式发送给客户。
     */
    public function __construct(string $client_id, string $username, string $password_md5, string $client_secret)
    {
        parent::__construct();
        $this->client_id = $client_id;
        $this->username = $username;
        $this->password_md5 = $password_md5;
        $this->client_secret = $client_secret;
        $this->timestamp = date("Y-m-d H:i:s");
        $this->grant_type = 'access_token';
    }


    /**
     * @return array
     */
    public function params(): array
    {
        $this->timestamp = date("Y-m-d H:i:s");
        $params = [
            'grant_type' => $this->grant_type,
            'client_id' => $this->client_id,
            'timestamp' => $this->timestamp,
            'username' => $this->username,
            'password' => $this->password_md5,
            'client_secret' => $this->client_secret,

        ];
        $params['sign'] = $this->sign();
        return $params;
    }

    /**
     * 签名
     * 生成规则如下：
     * 1.按照以下顺序将字符串拼接起来
     * client_secret+timestamp+client_id+username+password+grant_type+client_secret
     * 其中
     * client_secret的值是京东分配的，以邮件形式发送给客户。
     * timestamp与同名入参传值一致。
     * client_id与同名入参传值一致。
     * username与同名入参传值一致。
     * password，32位小写MD5值，与同名入参传值一致。
     * grant_type，与同名入参传值一致。
     * 2、将上述拼接的字符串使用32位md5加密，然后将结果转成大写进行传输。
     *
     * @return string
     */
    public function sign(): string
    {
        $str = $this->client_secret . $this->timestamp . $this->client_id
            . $this->username . $this->password_md5 . $this->grant_type . $this->client_secret;
        return strtoupper(md5($str));
    }

}