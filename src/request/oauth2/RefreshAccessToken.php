<?php

namespace jd_vop\request\oauth2;

use jd_vop\request\Request;

/**
 * 2.4 刷新 Access Token Request
 */
class RefreshAccessToken extends Request
{
    public static $uri = "oauth2/refreshToken";
    /**
     * @var string 授权时获取的refresh_token
     */
    protected $refresh_token;
    /**
     * @var string 即对接账号(由京东人员提供)
     */
    protected $client_id;
    /**
     * @var string client_secret的值是京东分配的，以邮件形式发送给客户。
     */
    protected $client_secret;

    /**
     * 2.4 刷新 Access Token Request
     * @param $refresh_token  string 授权时获取的refresh_token
     * @param $client_id string client_secret的值是京东分配的，以邮件形式发送给客户。
     * @param $client_secret string client_secret的值是京东分配的，以邮件形式发送给客户。
     */
    public function __construct(string $refresh_token, string $client_id, string $client_secret)
    {
        parent::__construct();
        $this->refresh_token = $refresh_token;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'refresh_token' => $this->refresh_token,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
        ];
    }
}