<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class SkuImage
 * @package jd_vop\request\product
 */
class SkuImage extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 商品编码，支持批量，用英文半角逗号隔开
     */
    protected $sku;
    /**
     * @var string 接口地址
     */
    protected static $uri = "api/product/skuImage";

    /**
     * 4.4 查询商品图片 Request
     * @param $token string 授权token
     * @param $sku string 商品编码，支持批量，用英文半角逗号隔开
     */
    public function __construct(string $token, string $sku)
    {
        parent::__construct();
        $this->token = $token;
        $this->sku = $sku;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'sku' => $this->sku
        ];
    }

}