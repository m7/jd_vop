<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

class ProCheck extends Request
{
    protected $token;
    protected $skuIds;
    protected $queryExts;
    protected static $uri = "api/product/check";

    /**
     * 4.6 验证商品可售性 request
     * @param $token string 授权token
     * @param $sku string 商品编码
     * @param $queryExts ProCheckQueryExt
     *
     */
    public function __construct( string $token, string $skuIds, ProCheckQueryExt $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->skuIds = $skuIds;
        $this->queryExts = $queryExts;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'skuIds' => $this->skuIds,
            'queryExts' => $this->queryExts->parse(),
        ];
    }

}