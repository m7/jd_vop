<?php

namespace jd_vop\request\product;

/**
 * Class GetDetailQueryExt
 * @package jd_vop\request\product
 */
class GetDetailQueryExt
{
    /**
     * @var string 移动端商品详情大字段
     */
    public $nappintroduction;
    /**
     * @var string PC端商品详情大字段
     */
    public $nintroduction;
    /**
     * @var string 微信小程序商品详情大字段，仅提供图片地址，需要客户添加显示逻辑
     */
    public $wxintroduction;
    /**
     * @var string 获取客户侧分类编号，需要京东运营维护京东SKU与客户分类编号的映射
     */
    public $contractSkuExt;
    /**
     * @var string 是否厂直商品
     */
    public $isFactoryShip;
    /**
     * @var string 是否节能环保商品
     */
    public $isEnergySaving;
    /**
     * @var string 京东侧税收分类编码
     */
    public $taxCode;
    /**
     * @var string 商品最低起购量
     */
    public $LowestBuy;
    /**
     * @var string 容量单位转换（例如：油品单位桶转升）
     */
    public $capacity;
    /**
     * @var string 京东侧模拟SPU号
     */
    public $spuId;
    /**
     * @var string SPU名称
     */
    public $pName;
    /**
     * @var string 是否京东配送
     */
    public $isJDLogistics;
    /**
     * @var string 商品税率
     */
    public $taxInfo;
    /**
     * @var string 商品69条码
     */
    public $upc69;
    /**
     * @var string 中国法分类（仅限图书商品使用）
     */
    public $ChinaCatalog;
    /**
     * @var string 商品池扩展字段
     */
    public $contractSkuPoolExt;
    /**
     * @var string 图书产品特色
     */
    public $productFeatures;
    /**
     * @var string 规格参数
     */
    public $seoModel;
    /**
     * @var string 获取结构化商品属性数据(同京东官网样式)
     */
    public $paramDetailJson;
    /**
     * @var string 转商详接口出参param为json格式(只解析原出参param)
     */
    public $paramJson;
    /**
     * @var string 质保信息
     */
    public $wserve;
    /**
     * @var string 是否自营
     */
    public $isSelf;
    /**
     * @var string 商品介绍
     */
    public $categoryAttrs;
    /**
     * @var string 颜色：color，尺码：size
     */
    public $saleAttr;
    /**
     * @var string 尺寸描述：长：length、宽：width、高：height
     */
    public $sizeDesc;

    /**
     * 移动端商品详情大字段
     */
    public function setNappintroduction()
    {
        $this->nappintroduction = 1;
    }

    /**
     * PC端商品详情大字段
     */
    public function setNintroduction()
    {
        $this->nintroduction = 1;
    }

    /**
     * 微信小程序商品详情大字段，仅提供图片地址，需要客户添加显示逻辑
     */
    public function setWxintroduction()
    {
        $this->wxintroduction = 1;
    }

    /**
     * 获取客户侧分类编号，需要京东运营维护京东SKU与客户分类编号的映射
     */
    public function setContractSkuExt()
    {
        $this->contractSkuExt = 1;
    }

    /**
     * 是否厂直商品
     */
    public function setIsFactoryShip()
    {
        $this->isFactoryShip = 1;
    }

    /**
     * 是否节能环保商品
     */
    public function setIsEnergySaving()
    {
        $this->isEnergySaving = 1;
    }

    /**
     * 京东侧税收分类编码
     */
    public function setTaxCode()
    {
        $this->taxCode = 1;
    }

    /**
     * 商品最低起购量
     */
    public function setLowestBuy()
    {
        $this->LowestBuy = 1;
    }

    /**
     * 容量单位转换（例如：油品单位桶转升）
     */
    public function setCapacity()
    {
        $this->capacity = 1;
    }

    /**
     * 京东侧模拟SPU号
     */
    public function setSpuId()
    {
        $this->spuId = 1;
    }

    /**
     * SPU名称
     */
    public function setPName()
    {
        $this->pName = 1;
    }

    /**
     * 是否京东配送
     */
    public function setIsJDLogistics()
    {
        $this->isJDLogistics = 1;
    }

    /**
     * 商品税率
     */
    public function setTaxInfo()
    {
        $this->taxInfo = 1;
    }

    /**
     * 商品69条码
     */
    public function setUpc69()
    {
        $this->upc69 = 1;
    }

    /**
     * 中国法分类（仅限图书商品使用）
     */
    public function setChinaCatalog()
    {
        $this->ChinaCatalog = 1;
    }

    /**
     * 商品池扩展字段
     */
    public function setContractSkuPoolExt()
    {
        $this->contractSkuPoolExt = 1;
    }

    /**
     * 图书产品特色
     */
    public function setProductFeatures()
    {
        $this->productFeatures = 1;
    }

    /**
     * 规格参数
     */
    public function setSeoModel()
    {
        $this->seoModel = 1;
    }

    /**
     * 获取结构化商品属性数据(同京东官网样式)
     */
    public function setParamDetailJson()
    {
        $this->paramDetailJson = 1;
    }

    /**
     * 转商详接口出参param为json格式(只解析原出参param)
     */
    public function setParamJson()
    {
        $this->paramJson = 1;
    }

    /**
     * 质保信息
     */
    public function setWserve()
    {
        $this->wserve = 1;
    }

    /**
     * 是否自营
     */
    public function setIsSelf()
    {
        $this->isSelf = 1;
    }

    /**
     * 商品介绍
     */
    public function setCategoryAttrs()
    {
        $this->categoryAttrs = 1;
    }

    /**
     * 颜色：color，尺码：size
     */
    public function setSaleAttr()
    {
        $this->saleAttr = 1;
    }

    /**
     * 尺寸描述：长：length、宽：width、高：heigh
     */
    public function setSizeDesc()
    {
        $this->sizeDesc = 1;
    }


    /**
     * @return string
     */
    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}