<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class GetDetail
 * @package jd_vop\request\product
 */
class GetDetail extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 商品编号（仅支持单个查询）
     */
    protected $sku;
    /**
     * @var GetDetailQueryExt
     */
    protected $queryExts;
    /**
     * @var string
     */
    protected static $uri = "api/product/getDetail";

    /**
     * 4.3 查询商品详情 Request
     * @param $token  string 授权token
     * @param $sku string 商品编号（仅支持单个查询）
     * @param $queryExts GetDetailQueryExt
     */
    public function __construct(string $token, string $sku, GetDetailQueryExt $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->sku = $sku;
        $this->queryExts = $queryExts;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'sku' => $this->sku,
            'queryExts' => $this->queryExts->parse(),
        ];
    }

}