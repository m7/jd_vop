<?php

namespace jd_vop\request\product;

/**
 * Class ProCheckQueryExt
 * @package jd_vop\request\product
 */
class ProCheckQueryExt
{
    /**
     * @var string 无理由退货类型
     */
    public $noReasonToReturn;
    /**
     * @var string 无理由退货文案类型
     */
    public $thwa;
    /**
     * @var string 是否自营
     */
    public $isSelf;
    /**
     * @var string 是否京东配送
     */
    public $isJDLogistics;
    /**
     * @var string 商品税率
     */
    public $taxInfo;

    /**
     * 无理由退货类型
     */
    public function setNoReasonToReturn()
    {
        $this->noReasonToReturn = 1;
    }
    /**
     * 无理由退货文案类型
     */
    public function setThwa()
    {
        $this->thwa = 1;
    }
    /**
     * 是否自营
     */
    public function setIsSelf()
    {
        $this->isSelf = 1;
    }
    /**
     * 是否京东配送
     */
    public function setIsJDLogistics()
    {
        $this->isJDLogistics = 1;
    }
    /**
     * 商品税率
     */
    public function setTaxInfo()
    {
        $this->taxInfo = 1;
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}