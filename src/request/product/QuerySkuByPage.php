<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class QuerySkuByPage
 * @package jd_vop\request\product
 */
class QuerySkuByPage extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var int 商品池编码 当前页数
     */
    protected $pageNum;
    /**
     * @var integer 当前页显示条数
     */
    protected $pageSize;
    /**
     * @var int 偏移量
     */
    protected $offset;
    /**
     * @var string 接口地址
     */
    protected static $uri = "api/product/querySkuByPage";

    /**
     * 4.2 查询池内商品编号 Request
     * @param $token string 授权token
     * @param $pageNum int 商品池编码 当前页数
     * @param $pageSize int 当前页显示条数
     * @param $offset int 偏移量
     */
    public function __construct(string $token, int $pageNum, int $pageSize, int $offset)
    {
        parent::__construct();
        $this->token = $token;
        $this->pageNum = $pageNum;
        $this->pageSize = $pageSize;
        $this->offset = $offset;
    }


    /**
     * @return array
     */
    public function params(): array
    {

        return [
            'token' => $this->token,
            'pageNum' => $this->pageNum,
            'pageSize' => $this->pageSize,
            'offset' => $this->offset,
        ];
    }

}