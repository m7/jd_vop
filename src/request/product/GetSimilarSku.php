<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class GetSimilarSku
 * @package jd_vop\request\product
 */
class GetSimilarSku extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 商品编码(单)
     */
    protected $skuId;
    /**
     * @var string
     */
    protected static $uri = "api/product/getSimilarSku";

    /**
     * 4.13 查询同类商品 Request
     * @param $token string 授权token
     * @param $skuId string 商品编码(单)
     */
    public function __construct($token, $skuId)
    {
        parent::__construct();
        $this->token = $token;
        $this->skuId = $skuId;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'skuId' => $this->skuId
        ];
    }

}