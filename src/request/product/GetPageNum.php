<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class GetPageNum
 * @package jd_vop\request\product
 */
class GetPageNum extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 接口地址
     */
    protected static $uri = "api/product/getPageNum";

    /**
     * 4.1 查询商品池编号    分类 Request
     * @param $token string 授权token
     */
    public function __construct(string $token)
    {
        parent::__construct();
        $this->token = $token;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return ['token' => $this->token];
    }

}