<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class SkuState
 * @package jd_vop\request\product
 */
class SkuState extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 商品编号，支持批量，以“,”（半角）分隔
     */
    protected $sku;
    /**
     * @var string 接口地址
     */
    protected static $uri = "api/product/skuState";

    /**
     * 4.5 查询商品 上下架状态 Request
     * @param $token string 授权token
     * @param $sku string 商品编号，支持批量，以“,”（半角）分隔
     */
    public function __construct($token, $sku)
    {
        parent::__construct();
        $this->token = $token;
        $this->sku = $sku;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'sku' => $this->sku
        ];
    }

}