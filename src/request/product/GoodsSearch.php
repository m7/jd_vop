<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class GoodsSearch
 * @package jd_vop\request\product
 */
class GoodsSearch extends Request
{
    /**
     * @var String 授权时获取的access token
     */
    protected $token;
    /**
     * @var string 搜索关键词
     */
    protected $Keyword;
    /**
     * @var string 分类ID 只支持第三级分类id
     */
    protected $catId;
    /**
     * @var int 当前第几页
     */
    protected $pageIndex;
    /**
     * @var int 当前页显示条数
     */
    protected $pageSize;
    /**
     * @var string 价格区间搜索，低价
     */
    protected $Min;
    /**
     * @var string 价格区间搜索，高价
     */
    protected $Max;
    /**
     * @var string 品牌搜索 多个品牌以逗号分隔，需要编码
     */
    protected $Brands;
    /**
     * @var string 一级分类
     */
    protected $cid1;
    /**
     * @var string 二级分类
     */
    protected $cid2;
    /**
     * @var string 排序 （例子："sale_desc"）
     */
    protected $sortType;
    /**
     * @var string 价格汇总 （例子：" yes"）
     */
    protected $priceCol;
    /**
     * @var string 扩展属性汇总 （例子：" yes"）
     */
    protected $extAttrCol;
    /**
     * @var string sku规格合并（true / false ）
     */
    protected $mergeSku;
    /**
     * @var string 接口地址
     */
    protected static $uri = "api/search/search";

    /**
     * GoodsSearch constructor.
     * @param $token String 授权时获取的access token
     * @param $Keyword string 搜索关键词
     * @param $catId string 分类ID 只支持第三级分类id
     * @param $pageIndex int 当前第几页
     * @param $pageSize int 当前页显示条数
     * @param $Min string 价格区间搜索，低价
     * @param $Max string 价格区间搜索，高价
     * @param $Brands string 品牌搜索 多个品牌以逗号分隔，需要编码
     * @param $cid1 string 一级分类
     * @param $cid2 string 二级分类
     * @param $sortType string 排序 （例子："sale_desc"）
     * @param $priceCol string  string 价格汇总 （例子：" yes"）
     * @param $extAttrCol string 扩展属性汇总 （例子：" yes"）
     * @param $mergeSku string sku规格合并（true / false ）
     */
    public function __construct(string $token,$Keyword,$catId,$pageIndex,$pageSize,$Min,$Max,$Brands,$cid1,$cid2,$sortType,$priceCol, $extAttrCol,$mergeSku)
    {
        parent::__construct();

        $this->token        = $token;
        $this->Keyword      = $Keyword;
        $this->catId        = $catId;
        $this->pageIndex    = $pageIndex;
        $this->pageSize     = $pageSize;
        $this->Min          = $Min;
        $this->Max          = $Max;
        $this->Brands       = $Brands;
        $this->cid1         = $cid1;
        $this->cid2         = $cid2;
        $this->sortType     = $sortType;
        $this->priceCol     = $priceCol;
        $this->extAttrCol   = $extAttrCol;
        $this->mergeSku     = $mergeSku;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $params = [
            'token'         => $this->token,
            'Keyword'       => $this->Keyword,
            'catId'         => $this->catId,
            'pageIndex'     => $this->pageIndex,
            'pageSize'      => $this->pageSize,
            'Min'           => $this->Min,
            'Max'           => $this->Max,
            'Brands'        => $this->Brands,
            'cid1'          => $this->cid1,
            'cid2'          => $this->cid2,
            'sortType'      => $this->sortType,
            'priceCol'      => $this->priceCol,
            'extAttrCol'    => $this->extAttrCol,
            'mergeSku'      => $this->mergeSku,
        ];

        foreach ($params as $key => $val){
            if(empty($val)) unset($params[$key]);
        }

        return $params;

    }

}