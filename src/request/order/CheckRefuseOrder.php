<?php

namespace jd_vop\request\order;

/**
 * 7.14 查询拒收订单列表 Request
 */
class CheckRefuseOrder extends CheckNewOrder
{
    protected static $uri = "api/checkOrder/checkRefuseOrder";

}