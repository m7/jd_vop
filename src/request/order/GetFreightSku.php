<?php

namespace jd_vop\request\order;

/**
 * 7.1 查询运费 sku
 */
class GetFreightSku
{
    /**
     * @var  int 商品编号
     */
    public $skuId;
    /**
     * @var int 商品数量
     */
    public $num;

    /**
     *  7.1 查询运费 sku
     * @param $skuId  int 商品编号
     * @param $num int 商品数量
     */
    public function __construct(int $skuId, int $num)
    {
        $this->skuId = $skuId;
        $this->num = $num;
    }

    /**
     * @return mixed
     */
    public function getSkuId()
    {
        return $this->skuId;
    }

    /**
     * @param mixed $skuId
     */
    public function setSkuId($skuId)
    {
        $this->skuId = $skuId;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param mixed $num
     */
    public function setNum($num)
    {
        $this->num = $num;
        if ($this->num < 0) {
            $this->num = 0;
        }
    }


}