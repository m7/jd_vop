<?php

namespace jd_vop\request\order;

/**
 * 7.1 查询运费skus数据
 */
class GetFreightSkus
{
    /**
     * @var array GetFreightSku 数组
     */
    public $list;


    /**
     * 7.1 查询运费skus数据
     */
    public function __construct()
    {
        $this->list = [];
    }

    /**
     * 添加商品
     * @param int $skuId 商品SKU
     * @param int $num 数量
     * @return bool
     */
    public function add(int $skuId, int $num = 1): bool
    {
        $flag = false;
        foreach ($this->list as $v) {
            if ($v instanceof GetFreightSku) {
                if ($v->getSkuId() == $skuId) {
                    $v->setNum($v->getNum() + $num);
                    $flag = true;
                    break;
                }
            }
        }
        if (!$flag) {
            if (count($this->list) < 50) {

                $this->list[] = new GetFreightSku($skuId, $num);
                $flag = true;
            }
        }
        return $flag;
    }

    /**
     * 减少商品
     * @param int $skuId 商品SKU
     * @param int $num 数量
     */
    public function subtract(int $skuId, int $num = 1)
    {
        foreach ($this->list as $k => $v) {
            if ($v instanceof GetFreightSku) {
                if ($v->getSkuId() == $skuId) {
                    $v->setNum($v->getNum() - $num);
                    if ($v->getNum() <= 0) {
                        array_splice($this->list, $k, 1);
                    }
                    break;
                }
            }
        }
    }

    /**
     * 解析数据
     * @return false|string
     */
    public function parse()
    {
        return json_encode($this->list);
    }
}