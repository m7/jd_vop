<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.12 查询新建订单列表 Request
 */
class CheckNewOrder extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/checkOrder/checkNewOrder";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string  查询日期，格式2018-11-7（不包含当天）
     */
    public $date;
    /**
     * @var int 页码，默认1
     */
    public $pageNo;
    /**
     * @var int 页大小取值范围[1,100]，默认20
     */
    public $pageSize;
    /**
     * @var int 最小订单号索引游标，为解决大于1W条订单无法查询问题。
     * 注意事项：该字段和pageNo互斥，订单数小于1W可以用pageNo分页的方式来查询，订单数大于1W则需要使用索引游标的方式来读取数据。
     * 使用方式：第一次查询无需传入该字段，返回订单信息后（第一次记录订单总条数）；第二次查询将第一次查询结果中最小的订单号传入，查询返回结果中不包含传入的订单号；递归这个流程，直到接口返回无数据为止，订单查询完毕，核对本地订单数和第一次接口返回的订单数目是否一致。
     * 如果使用本字段：订单号必须大于1
     */
    public $jdOrderIdIndex;
    /**
     * @var string 结束日期，格式2018-11-7。主要用于查询时间段内，跟date配合使用。
     */
    public $endDate;

    /**
     * 7.12 查询新建订单列表 Request
     * @param string $token access token
     * @param string $date 查询日期
     * @param mixed $pageNo 页码
     * @param mixed $pageSize 页大小取值范围
     * @param mixed $jdOrderIdIndex 最小订单号索引游标
     * @param mixed $endDate 结束日期
     */
    public function __construct(string $token, string $date,  $pageNo,  $pageSize, $jdOrderIdIndex, $endDate)
    {
        parent::__construct();
        $this->token = $token;
        $this->date = $date;
        $this->pageNo = $pageNo;
        $this->pageSize = $pageSize;
        $this->jdOrderIdIndex = $jdOrderIdIndex;
        $this->endDate = $endDate;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        $data = [
            'token' => $this->token,
            'date' => $this->date,
            'pageNo' => $this->pageNo,
            'pageSize' => $this->pageSize,
            'jdOrderIdIndex' => $this->jdOrderIdIndex,
            'endDate' => $this->endDate,
        ];

        if (empty($this->jdOrderIdIndex)) {
            unset($data['jdOrderIdIndex']);
        }
        if (empty($this->endDate)) {
            unset($data['endDate']);
        }
        return $data;
    }
}