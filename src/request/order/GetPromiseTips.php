<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.16 查询配送预计送达时间 Request
 */
class GetPromiseTips extends Request
{
    /**
     * @var string
     */
    protected static $uri = "api/order/getPromiseTips";

    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string $skuId 商品编号
     */
    public $skuId;
    /**
     * @var string $num 数量
     */
    public $num;
    /**
     * @var  int 一级地址id
     */
    public $province;
    /**
     * @var int 二级地址id
     */
    public $city;
    /**
     * @var  int 三级地址id
     */
    public $county;
    /**
     * @var  int 四级地址id
     */
    public $town;

    /**
     * 7.16 查询配送预计送达时间 Request
     * @param $token string access token
     * @param string $skuId 商品编号
     * @param string $num 数量
     * @param $province int 一级地址id
     * @param $city int 二级地址id
     * @param $county int 三级地址id
     * @param $town int 四级地址id
     */
    public function __construct(string $token, string $skuId, string $num, int $province, int $city, int $county, int $town)
    {
        parent::__construct();
        $this->token = $token;
        $this->skuId = $skuId;
        $this->num = $num;
        $this->province = $province;
        $this->city = $city;
        $this->county = $county;
        $this->town = $town;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'skuId' => $this->skuId,
            'num' => $this->num,
            'province' => $this->province,
            'city' => $this->city,
            'county' => $this->county,
            'town' => $this->town,
        ];
    }
}