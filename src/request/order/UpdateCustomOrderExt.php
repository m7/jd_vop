<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.11 更新订单扩展字段 Request
 * Class UpdateCustomOrderExt
 * @package jd_vop\request\product
 */
class UpdateCustomOrderExt extends Request
{
    /**
     * @var String 授权时获取的access token
     */
    protected $token;
    /**
     * @var String 订单号,更新扩展字段时以订单号作为唯一区分，拆单需要传子单号
     */
    protected $jdOrderId;
    /**
     * @var  CustomExt $customOrderExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     */
    protected $customOrderExt;
    /**
     * @var  CustomExt $customSkuExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     */
    protected $customSkuExt;

    /**
     * @var string
     */
    protected static $uri = "api/order/updateCustomOrderExt";


    /**
     * 7.11 更新订单扩展字段 Request
     * UpdateCustomOrderExt constructor.
     * @param $token
     * @param $jdOrderId  订单号,更新扩展字段时以订单号作为唯一区分，拆单需要传子单号
     * @param CustomExt $customOrderExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     * @param CustomExt $customSkuExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     */
    public function __construct($token, $jdOrderId, CustomExt $customOrderExt, CustomExt $customSkuExt)
    {
        parent::__construct();

        $this->token    = $token;
        $this->jdOrderId = $jdOrderId;
        $this->customOrderExt   = $customOrderExt;
        $this->customSkuExt   = $customSkuExt;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        $params = [
            'token'     => $this->token,
            'jdOrderId'  => $this->jdOrderId,
            'customOrderExt'    => $this->customOrderExt->parse(),
            'customSkuExt'    => $this->customSkuExt->parse(),
        ];
        var_dump($params);die;
        return $params;
    }

}