<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.3 提交订单 request
 */
class SubmitOrder extends Request
{
    /**
     * @var string
     */
    protected static $uri = "api/order/submitOrder";

    /**
     * @var string access_token
     */
    public $token;
    /**
     * @var string 第三方的订单单号，必须在100字符以内
     */
    public $thirdOrder;
    /**
     * @var SubmitOrderSkus 下单商品信息
     */
    public $skus;
    /**
     * @var string 收货人姓名，最多20个字符
     */
    public $name;
    /**
     * @var int 一级地址编码：收货人省份地址编码
     */
    public $province;
    /**
     * @var int 二级地址编码：收货人市级地址编码
     */
    public $city;
    /**
     * @var int  三级地址编码：收货人县（区）级地址编码
     */
    public $county;
    /**
     * @var int 四级地址编码：收货人乡镇地址编码(如果该地区有四级地址，则必须传递四级地址，没有四级地址则传0)
     */
    public $town;
    /**
     * @var string 收货人详细地址，最多100个字符
     */
    public $address;
    /**
     * @var string 邮编，最多20个字符
     */
    public $zip;
    /**
     * @var string  座机号，最多20个字符
     */
    public $phone;
    /**
     * @var string 手机号，最多20个字符
     */
    public $mobile;
    /**
     * @var string 邮箱（接口需要，无实际业务意义，可固值xx）
     */
    public $email; //
    /**
     * @var string 备注（少于100字）
     */
    public $remark; //
    /**
     * @var int 开票方式(2为集中开票，4 订单完成后开票)
     */
    public $invoiceState; //
    /**
     * @var  int 发票类型（2增值税专用发票；3 电子票）
     * 当发票类型为2时，开票方式只支持2集中开票
     */
    public $invoiceType; //
    /**
     * @var int 发票类型：4：个人，5：单位
     */
    public $selectedInvoiceTitle; //
    /**
     * @var string 发票抬头  (如果selectedInvoiceTitle=5则此字段必须) 需regCompanyName跟此字段传递一致
     */
    public $companyName; //
    /**
     * @var int 1:明细，100：大类 备注:若增值税专用发票则只能选1 明细
     */
    public $invoiceContent; //
    /**
     * @var int 支付方式枚举值
     */
    public $paymentType;
    /**
     * @var string 支付明细，
     * 当paymentType为20时候必须递此字段
     */
    public $payDetails;
    /**
     * @var int 使用余额paymentType=4时，此值固定是1 其他支付方式0
     */
    public $isUseBalance;
    /**
     * @var int 是否预占库存，0是预占库存（需要调用确认订单接口），1是不预占库存，直接进入生产
     */
    public $submitState;
    /**
     * @var string 增专票收票人姓名
     */
    public $invoiceName;
    /**
     * @var string 收票人电话
     */
    public $invoicePhone;
    /**
     * @var int 增专票收票人所在省(京东地址编码)
     */
    public $invoiceProvice;
    /**
     * @var int 增专票收票人所在市(京东地址编码)
     */
    public $invoiceCity;
    /**
     * @var int 增专票收票人所在区/县(京东地址编码)
     */
    public $invoiceCounty;
    /**
     * @var string 增专票收票人所在地址当invoiceType =2时，选填。
     */
    public $invoiceAddress;
    /**
     * @var string 专票资质公司名称
     */
    public $regCompanyName;
    /**
     * @var string 专票资质纳税人识别号
     */
    public $regCode;
    /**
     * @var string 专票资质注册地址
     */
    public $regAddr;
    /**
     * @var string 专票资质注册电话
     */
    public $regPhone;
    /**
     * @var string 专票资质注册银行
     */
    public $regBank;
    /**
     * @var string 专票资质银行账号
     */
    public $regBankAccount;
    /**
     * @var int 大家电配送日期：默认值为-1，0表示当天，1表示明天，2：表示后天; 如果为-1表示不使用大家电预约日历
     */
    public $reservingDate;
    /**
     * @var int 大家电安装日期：默认按-1处理，0表示当天，1表示明天，2：表示后天
     */
    public $installDate;
    /**
     * @var bool 是否选择了安装，默认为true，选择了“暂缓安装”，此为必填项，必填值为false。
     */
    public $needInstall;
    /**
     * @var string 中小件配送预约日期，格式：yyyy-MM-dd
     */
    public $promiseDate;
    /**
     * @var string 中小件配送预约时间段，时间段如： 9:00-15:00
     */
    public $promiseTimeRange;
    /**
     * @var int 中小件预约时间段的标记
     */
    public $promiseTimeRangeCode;
    /**
     * @var string 家电配送预约日期，格式：yyyy-MM-dd
     */
    public $reservedDateStr;
    /**
     * @var string 大家电配送预约时间段，如果：9:00-15:00
     */
    public $reservedTimeRange;
    /**
     * @var string 循环日历, 客户传入最近一周可配送的时间段,客户入参:{"3": "09:00-10:00,12:00-19:00","4": "09:00-15:00"}
     */
    public $cycleCalendar;
    /**
     * @var string 采购单号，长度范围[1-26]
     */
    public $poNo;
    /**
     * @var bool 节假日不可配送，默认值为false，表示节假日可以配送，为true表示节假日不配送
     */
    public $validHolidayVocation;
    /**
     * @var string 第三方平台采购人账号
     */
    public $thrPurchaserAccount;
    /**
     * @var string 第三方平台采购人姓名
     */
    public $thrPurchaserName;
    /**
     * @var string 第三方采购人电话（手机号，固定电话区号+号码）
     */
    public $thrPurchaserPhone;
    /**
     * @var array 对于有订单维度扩展字段需求的用户，提交订单时可以定义扩展字段信息，key需要提前申请开通，
     */
    public $customOrderExt;

    /**
     * 7.3 提交订单 request
     * 需要全部必填参数
     * @param string $token access_token
     * @param string $thirdOrder 第三方的订单单号，必须在100字符以内
     * @param SubmitOrderSkus $skus 下单商品信息
     * @param string $name 收货人姓名，最多20个字符
     * @param int $province 一级地址编码：收货人省份地址编码
     * @param int $city 二级地址编码：收货人市级地址编码
     * @param int $county 三级地址编码：收货人县（区）级地址编码
     * @param int $town 四级地址编码：收货人乡镇地址编码(如果该地区有四级地址，则必须传递四级地址，没有四级地址则传0)
     * @param string $address 收货人详细地址，最多100个字符
     * @param string $mobile 手机号，最多20个字符
     * @param int $invoiceState 开票方式(2为集中开票，4 订单完成后开票)
     * todo 改枚举
     * @param int $invoiceType int 发票类型（2增值税专用发票；3 电子票）
     * 当发票类型为2时，开票方式只支持2集中开票
     * todo 改枚举
     * @param int $selectedInvoiceTitle 发票抬头  (如果selectedInvoiceTitle=5则此字段必须) 需regCompanyName跟此字段传递一致
     * @param int $invoiceContent 1:明细，100：大类 备注:若增值税专用发票则只能选1 明细
     * todo 改枚举
     * @param int $paymentType 支付方式枚举值  constant\PaymentType
     * @param int $isUseBalance 使用余额paymentType=4时，此值固定是1 其他支付方式0
     * todo 改枚举
     * @param int $submitState 是否预占库存，0是预占库存（需要调用确认订单接口），1是不预占库存，直接进入生产
     * todo 改枚举
     * @param string $invoicePhone 收票人电话
     * @param string $regCompanyName 专票资质公司名称
     * @param string $regCode 专票资质纳税人识别号
     */
    public function __construct(string $token, string $thirdOrder, SubmitOrderSkus $skus, string $name, int $province, int $city, int $county, int $town, string $address, string $mobile, int $invoiceState, int $invoiceType, int $selectedInvoiceTitle, int $invoiceContent, int $paymentType, int $isUseBalance, int $submitState, string $invoicePhone, string $regCompanyName, string $regCode)
    {
        parent::__construct();
        $this->token = $token;
        $this->thirdOrder = $thirdOrder;
        $this->skus = $skus;
        $this->name = $name;
        $this->province = $province;
        $this->city = $city;
        $this->county = $county;
        $this->town = $town;
        $this->address = $address;
        $this->mobile = $mobile;
        $this->invoiceState = $invoiceState;
        $this->invoiceType = $invoiceType;
        $this->selectedInvoiceTitle = $selectedInvoiceTitle;
        $this->invoiceContent = $invoiceContent;
        $this->paymentType = $paymentType;
        $this->isUseBalance = $isUseBalance;
        $this->submitState = $submitState;
        $this->invoicePhone = $invoicePhone;
        $this->regCompanyName = $regCompanyName;
        $this->regCode = $regCode;
    }


    public function params(): array
    {
        return [
            'token' => $this->token,
            'thirdOrder' => $this->thirdOrder,
            'sku' => $this->skus->parse(),
            'name' => $this->name,
            'province' => $this->province,
            'city' => $this->city,
            'county' => $this->county,
            'town' => $this->town,
            'address' => $this->address,
            'zip' => $this->zip,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'remark' => $this->remark,
            'invoiceState' => $this->invoiceState,
            'invoiceType' => $this->invoiceType,
            'selectedInvoiceTitle' => $this->selectedInvoiceTitle,
            'companyName' => $this->companyName,
            'invoiceContent' => $this->invoiceContent,
            'paymentType' => $this->paymentType,
            'payDetails' => $this->payDetails,
            'isUseBalance' => $this->isUseBalance,
            'submitState' => $this->submitState,
            'invoiceName' => $this->invoiceName,
            'invoicePhone' => $this->invoicePhone,
            'invoiceProvice' => $this->invoiceProvice,
            'invoiceCity' => $this->invoiceCity,
            'invoiceCounty' => $this->invoiceCounty,
            'invoiceAddress' => $this->invoiceAddress,
            'regCompanyName' => $this->regCompanyName,
            'regCode' => $this->regCode,
            'regAddr' => $this->regAddr,
            'regPhone' => $this->regPhone,
            'regBank' => $this->regBank,
            'regBankAccount' => $this->regBankAccount,
            'reservingDate' => $this->reservingDate,
            'installDate' => $this->installDate,
            'needInstall' => $this->needInstall,
            'promiseDate' => $this->promiseDate,
            'promiseTimeRange' => $this->promiseTimeRange,
            'promiseTimeRangeCode' => $this->promiseTimeRangeCode,
            'reservedDateStr' => $this->reservedDateStr,
            'reservedTimeRange' => $this->reservedTimeRange,
            'cycleCalendar' => $this->cycleCalendar,
            'validHolidayVocation' => $this->validHolidayVocation,
            'poNo' => $this->poNo,
            'thrPurchaserAccount' => $this->thrPurchaserAccount,
            'thrPurchaserName' => $this->thrPurchaserName,
            'thrPurchaserPhone' => $this->thrPurchaserPhone,
            'customOrderExt' => $this->customOrderExt,
        ];
    }


}