<?php

namespace jd_vop\request\order;
/**
 * 7.15 查询完成订单列表 Request
 */
class CheckCompleteOrder extends CheckNewOrder
{

    protected static $uri = "api/checkOrder/checkCompleteOrder";
}