<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.7 查询订单详情 Request
 */
class SelectJdOrder extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/order/selectJdOrder";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;
    /**
     * @var SelectJdOrderQueryExt  扩展参数
     */
    public $queryExts;

    /**
     * 7.7 查询订单详情 Request
     * @param string $token  access token
     * @param int $jdOrderId  京东的订单单号(下单返回的父订单号)
     * @param SelectJdOrderQueryExt $queryExts  扩展参数
     */
    public function __construct(string $token, int $jdOrderId, SelectJdOrderQueryExt $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
        $this->queryExts = $queryExts;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
            'queryExts'=>$this->queryExts->parse()
        ];
    }

}