<?php

namespace jd_vop\request\order;

/**
 * 7.11 更新订单扩展字段
 */
/**
 * Class CustomExt
 * @package jd_vop\request\order
 */
class CustomExt
{
    /**
     * @var array CustomExt 数组
     */
    public $list;


    /**
     * 7.11 更新订单扩展字段
     */
    public function __construct()
    {
        $this->list = [];
    }


    /**
     * @param string $key 扩展字段信息，key需要提前申请开通
     * @param string $val
     */
    public function orderAdd(string $key, string $val): void
    {
        $this->list[$key] = $val;

    }

    /**
     * @param string $key 扩展字段信息，key需要提前申请开通
     * @param string $val
     * @param string $skuId sku维度扩展字段
     */
    public function skuAdd(string $key, string $val, string $skuId): void
    {

        $this->list[$skuId][$key] = $val;
    }

    /**
     * 解析数据
     * @return false|string
     */
    public function parse()
    {
        return $this->list;
    }
}