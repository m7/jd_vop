<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.9 确认收货 Request
 */
class ConfirmReceived extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/order/confirmReceived";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;

    /**
     * 7.9 确认收货 Request
     * @param string $token access token
     * @param string $jdOrderId 京东的订单单号(下单返回的父订单号)
     */
    public function __construct(string $token, string $jdOrderId)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
        ];
    }

}