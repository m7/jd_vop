<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.2 检查预约日历 Request
 * Class PromiseCalendar
 * @package jd_vop\request\product
 */
class PromiseCalendar extends Request
{
    /**
     * @var String 授权时获取的access token
     */
    protected $token;
    /**
     * @var String 一级地址编号
     */
    protected $province;
    /**
     * @var String 二级地址编号
     */
    protected $city;
    /**
     * @var String 三级地址编号
     */
    protected $county;
    /**
     * @var String 四级地址编号（有则必传，没有默认是 0 ）
     */
    protected $town;
    /**
     * @var Int 支付类型,合同上允许的支付类型都可以
     */
    protected $paymentType;
    /**
     * @var  GetFreightSkus 商品集合
     */
    protected $skuIds;

    /**
     * @var string
     */
    protected static $uri = "api/order/promiseCalendar";

    /**
     * 7.2 检查预约日历 Request
     * TotalCheckNew constructor.
     * @param $token String 授权时获取的access token
     * @param $province String 一级地址编号
     * @param $city String 二级地址编号
     * @param $county String 三级地址编号
     * @param $town String 四级地址编号（有则必传，没有默认是 0 ）
     * @param $paymentType Int 支付类型
     * @param GetFreightSkus $skuIds String 商品编号
     */
    public function __construct($token, $province, $city, $county, $town, $paymentType, GetFreightSkus $skuIds)
    {
        parent::__construct();

        $this->token    = $token;
        $this->province = $province;
        $this->city     = $city;
        $this->county   = $county;
        $this->town     = $town ?? 0;
        $this->paymentType   = $paymentType;
        $this->skuIds   = $skuIds;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token'     => $this->token,
            'province'  => $this->province,
            'city'      => $this->city,
            'county'    => $this->county,
            'town'      => $this->town,
            'paymentType'          => $this->paymentType,
            'sku'    => $this->skuIds->parse(),
        ];
    }

}