<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.1 查询运费 Request
 */
class GetFreight extends Request
{
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var GetFreightSkus sku数组
     */
    public $skus;
    /**
     * @var int 一级地址id
     */
    public $province;
    /**
     * @var int 二级地址id
     */
    public $city;
    /**
     * @var int 三级地址id
     */
    public $county;
    /**
     * @var int 四级地址id
     */
    public $town;
    /**
     * @var int 京东支付方式 constant\PaymentType
     */
    public $paymentType;
    /**
     * @var string 扩展查询字段,  conFreight //续重运费
     */
    public $queryExts;
    /**
     * @var string
     */
    protected static $uri = "api/order/getFreight";

    /**
     * 7.1 查询运费 Request
     * @param $token string access token
     * @param GetFreightSkus $skus  sku数组
     * @param $province int 一级地址id
     * @param $city int 二级地址id
     * @param $county int 三级地址id
     * @param $town int 四级地址id
     * @param $paymentType  int 京东支付方式 constant\PaymentType
     * @param $queryExts string 扩展查询字段,  conFreight //续重运费  todo 修改为扩展类库
     */
    public function __construct(string $token, GetFreightSkus $skus, int $province, int $city, int $county, $town, int $paymentType, string $queryExts)
    {
        parent::__construct();
        $this->token = $token;
        $this->skus = $skus;
        $this->province = $province;
        $this->city = $city;
        $this->county = $county;
        $this->town = $town;
        $this->paymentType = $paymentType;
        $this->queryExts = $queryExts;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'sku' => $this->skus->parse(),
            'province' => $this->province,
            'city' => $this->city,
            'county' => $this->county,
            'town' => $this->town,
            'paymentType' => $this->paymentType,
            'queryExts' => $this->queryExts,
        ];
    }

}