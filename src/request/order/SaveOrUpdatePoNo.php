<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.10 更新采购单号  Request
 */
class SaveOrUpdatePoNo extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/order/saveOrUpdatePoNo";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;
    /**
     * @var string 采购单号，长度范围[1-26]
     */
    public $poNo;

    /**
     * 7.10 更新采购单号  Request
     * @param string $token  access token
     * @param string $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param string $poNo 采购单号，长度范围[1-26]
     */
    public function __construct(string $token, string $jdOrderId, string $poNo)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
        $this->poNo = $poNo;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
            'poNo' => $this->poNo
        ];
    }

}