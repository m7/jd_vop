<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.13 查询妥投订单列表 Request
 */
class CheckDlokOrder extends CheckNewOrder
{

    /**
     * @var string
     */
    protected static $uri = "api/checkOrder/checkDlokOrder";

}