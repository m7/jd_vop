<?php

namespace jd_vop\request\order;

class SelectJdOrderQueryExt
{

    /**
     * @var int 订单类型
     */
    public $orderType;
    /**
     * @var int 京东订单状态
     */
    public $jdOrderState;
    /**
     * @var int 采购单号
     */
    public $poNo;
    /**
     * @var int 订单完成时间
     */
    public $finishTime;
    /**
     * @var int 订单创建时间
     */
    public $createOrderTime;
    /**
     * @var int 订单支付类型
     */
    public $paymentType;
    /**
     * @var int 订单出库时间
     */
    public $outTime;
    /**
     * @var int 订单发票类型
     */
    public $invoiceType;

    /**
     * 订单类型
     */
    public function setOrderType(): void
    {
        $this->orderType = 1;
    }

    /**
     * 京东订单状态
     */
    public function setJdOrderState(): void
    {
        $this->jdOrderState = 1;
    }

    /**
     * 采购单号
     */
    public function setPoNo(): void
    {
        $this->poNo = 1;
    }

    /**
     * 订单完成时间
     */
    public function setFinishTime(): void
    {
        $this->finishTime = 1;
    }

    /**
     * 订单创建时间
     */
    public function setCreateOrderTime(): void
    {
        $this->createOrderTime = 1;
    }

    /**
     * 订单支付类型
     */
    public function setPaymentType(): void
    {
        $this->paymentType = 1;
    }

    /**
     * 订单出库时间
     */
    public function setOutTime(): void
    {
        $this->outTime = 1;
    }

    /**
     * 订单发票类型
     */
    public function setInvoiceType(): void
    {
        $this->invoiceType = 1;
    }


    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}