<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.3 查询三级地址 Request
 */
class County extends Request
{
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 二级地址ID
     */
    public $city_id;
    /**
     * @var string
     */
    protected static $uri = "api/area/getCounty";

    /**
     * 3.3 查询三级地址 Request
     * @param $token string access token
     * @param $city_id int 二级地址ID
     */
    public function __construct(string $token, int $city_id)
    {
        parent::__construct();
        $this->token = $token;
        $this->city_id = $city_id;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'id' => $this->city_id,
        ];
    }
}