<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.6 地址详情转换京东地址编码 Request
 */
class GetJDAddressFromAddress extends Request
{
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string 详细地址
     */
    public $address;
    /**
     * @var string
     */
    protected static $uri = "api/area/getJDAddressFromAddress";

    /**
     * 3.6 地址详情转换京东地址编码 Request
     * @param $token string access token
     * @param $address  string 详细地址
     */
    public function __construct(string $token, string $address)
    {
        parent::__construct();
        $this->token = $token;
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'address' => $this->address,
        ];
    }


}