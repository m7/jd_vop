<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.4 查询四级地址 Request
 */
class Town extends Request
{
    /**
     * @var string access token
     */
    public $token;

    /**
     * @var int 三级地址id
     */
    public $county_id;
    /**
     * @var string
     */
    protected static $uri = "api/area/getTown";

    /**
     * 3.4 查询四级地址 Request
     * @param $token string access token
     * @param $county_id  int 三级地址id
     */
    public function __construct(string $token, int $county_id)
    {
        parent::__construct();
        $this->token = $token;
        $this->county_id = $county_id;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'id' => (string)$this->county_id,
        ];
    }
}