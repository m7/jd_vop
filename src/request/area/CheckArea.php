<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.5 验证地址有效性 Request
 */
class CheckArea extends Request
{

    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 一级地址ID
     */
    public $province_id;
    /**
     * @var int 二级地址id
     */
    public $city_id;
    /**
     * @var int 三级地址id
     */
    public $county_id;
    /**
     * @var int 四级地址ID 若三级地址下无四级地址传0
     */
    public $town_id;
    /**
     * @var string
     */
    protected static $uri = "api/area/checkArea";

    /**
     * 3.5 验证地址有效性 Request
     * @param $token  string access token
     * @param $province_id int 一级地址ID
     * @param $city_id int 二级地址id
     * @param $county_id  int 三级地址id
     * @param $town_id  int 四级地址ID 若三级地址下无四级地址传0
     */
    public function __construct(string $token, int $province_id, int $city_id, int $county_id, int $town_id)
    {
        parent::__construct();
        $this->token = $token;
        $this->province_id = $province_id;
        $this->city_id = $city_id;
        $this->county_id = $county_id;
        $this->town_id = $town_id;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'provinceId' => $this->province_id,
            'cityId' => $this->city_id,
            'countyId' => $this->county_id,
            'townId' => $this->town_id,
        ];
    }


}