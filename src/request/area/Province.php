<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.1 查询一级地址 Request
 */
class Province extends Request
{
    /**
     * @var string access token
     */
    protected $token;
    protected static $uri = "api/area/getProvince";

    /**
     * 3.1 查询一级地址 Request
     * @param $token  string access token
     */
    public function __construct(string $token)
    {
        parent::__construct();
        $this->token = $token;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return ['token' => $this->token];
    }

}