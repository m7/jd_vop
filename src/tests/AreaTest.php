<?php

namespace jd_vop\tests;

use jd_vop\Area;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

class AreaTest extends TestCase
{

    protected $area;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->area = new Area($client_id, $username, $password_md5, $client_secret);
    }


    /**
     * 3.1 查询一级地址
     */
    public function testProvince()
    {
        $result = $this->area->Province(Env::get("access_token"));
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 3.2 查询二级地址
     */
    public function testCity()
    {
        $result = $this->area->City(Env::get("access_token"), 5);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 3.3 查询三级地址
     */
    public function testCounty()
    {
        $result = $this->area->County(Env::get("access_token"), 258);
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    /**
     * 3.4 查询四级地址
     */
    public function testTown()
    {
        $result = $this->area->Town(Env::get("access_token"), 41497);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 3.5 验证地址有效性
     */
    public function testCheckArea()
    {
        $result = $this->area->CheckArea(Env::get("access_token"), 5, 258, 41497, 41868);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 3.6 地址详情转换京东地址编码
     */
    public function testGetJDAddressFromAddress()
    {
        $result = $this->area->GetJDAddressFromAddress(Env::get("access_token"), "河北省唐山市路北区唐山科技中心");
        $this->assertNotEmpty($result);
        var_dump($result);
    }


}
