<?php

namespace jd_vop\tests;

use jd_vop\Oauth2;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

class Oauth2Test extends TestCase
{

    protected $oauth2;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->oauth2 = new Oauth2($client_id, $username, $password_md5, $client_secret);
    }


    /**
     * 2.3 获取Access Token
     */
    public function testGetAccessToken()
    {
//        $this->expectOutputRegex('/success/');
//        $this->expectException(\Exception::class);

        $result = $this->oauth2->AccessToken();
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 2.4 刷新 Access Token
     */
    public function testRefreshAccessToken()
    {
        $result = $this->oauth2->RefreshAccessToken(Env::get("refresh_token"));
        $this->assertNotEmpty($result);
        var_dump($result);
    }


}
