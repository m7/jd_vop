<?php

namespace jd_vop\tests;

use jd_vop\Area;
use jd_vop\Message;
use jd_vop\request\message\GetType;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{

    protected $message;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->message = new Message($client_id, $username, $password_md5, $client_secret);
    }


    /**
     * 11.1 查询推送信息
     */
    public function testGet()
    {
        $type = new GetType();
        $result = $this->message->Get(Env::get("access_token"), $type->setAll());
        $this->assertNotEmpty($result);
        var_dump($result);
        // 6389425000028835,6389425000028829
    }

    /**
     * 11.2 删除推送信息
     */
    public function testDel()
    {
        $result = $this->message->Del(Env::get("access_token"), "6389425000028835,6389425000028829");
        $this->assertNotEmpty($result);
        var_dump($result);
        // 6389425000028835,6389425000028829
    }

}