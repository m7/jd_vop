<?php

namespace jd_vop\tests;

use jd_vop\Stock;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;


/**
 * Class StockTest
 * @package jd_vop\tests
 */
class StockTest extends TestCase
{

    /**
     * @var Stock
     */
    protected $stock;

    /**
     * StockTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->stock = new Stock($client_id, $username, $password_md5, $client_secret);


    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetSellPrice()
    {
        $result = $this->stock->GetNewStockById(Env::get("access_token"), "[{skuId: 114942,num:2}]", '13_1000_4277_0');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    //价格接口结束//

}