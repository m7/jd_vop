<?php

namespace jd_vop\tests;

use jd_vop\AfterSale;
use jd_vop\request\aftersale\GetAfsOutlinQueryExt;
use jd_vop\request\aftersale\UpdateSendInfoWaybills;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;


class AfterSaleTest extends TestCase
{

    protected $aftersale;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->aftersale = new AfterSale($client_id, $username, $password_md5, $client_secret);


    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetSellPrice()
    {
        $result = $this->aftersale->GetGoodsAttributes(Env::get("access_token"), "234473500394", '众城VOP','7321529');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testCreateAfsApply()
    {
        $result = $this->aftersale->CreateAfsApply(Env::get("access_token"), "113184086705", '阴伟','123184086705_1',
            true,'阴伟','阴伟','4006066866',
            '','','10000','4','1',
            '2810','51081','0','北京市大兴区京东总部4号楼','','',
            '10','1','2810','51081','0','北京市大兴区京东总部4号楼',
            '10',false,false,false,'1','测试订单售后',
            '','4682026','4682026','得力(deli)0.5mm办公中性笔 水笔签字笔 12支/盒黑色34567','1',
            '测试商品申请退货',10.01,'10');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.3 查询售后概要
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetAfsOutline(){
        $queryExt = new GetAfsOutlinQueryExt();
        $result = $this->aftersale->GetAfsOutline(Env::get("access_token"), "",'113184086705','','','', '','','',$queryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.4 填写运单信息
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testUpdateSendInfo(){
        $waybillInfoList = new UpdateSendInfoWaybills();
        $waybillInfoList->add('测试运单信息接口','13312332132112','2021-12-16 09:01:44','10.50','123123','10',1);
        $waybillInfoList->add('测试运单信息接口','133123321321123','2021-12-16 09:01:44','10.50','123123','10',1);
        $request = new \jd_vop\request\aftersale\UpdateSendInfo(Env::get("access_token"),'','113184086705','113184086705',$waybillInfoList);
        $result = $this->aftersale->UpdateSendInfo($request);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.5 确认售后完成
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testConfirmAfsOrder(){
        $result = $this->aftersale->ConfirmAfsOrder(Env::get("access_token"), '113184086705','','');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.6取消售后申请
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testCancelAfsApply(){
        $result = $this->aftersale->CancelAfsApply(Env::get("access_token"), "",'113184086705','123184086705_1','');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.7 查询售后申请单明细
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetApplyDetailInfo(){
        $result = $this->aftersale->GetApplyDetailInfo(Env::get("access_token"), '','113184086705','','1,2');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 9.8查询退款明细
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetOrderPayByOrderId(){
        $result = $this->aftersale->GetOrderPayByOrderId(Env::get("access_token"), '113184086705','','','');
        $this->assertNotEmpty($result);
        var_dump($result);
    }

}