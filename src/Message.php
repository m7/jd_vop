<?php

namespace jd_vop;

use jd_vop\response\message\Get;
use jd_vop\response\message\ResultList;

class Message extends Jd_vop
{
    /**
     * 11.1 查询推送信息
     * @param string $token
     * @param $type
     * @return mixed|null
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function Get(string $token, $type)
    {
        $request = new request\message\Get($token, $type);
        $response = $this->Req($request, ResultList::class);

        return $response->result;
    }

    /**
     * 11.2 删除推送信息
     * @param string $token
     * @param string $id message\get 中获取的id，支持批量删除，英文逗号间隔，最大100个
     * @return mixed|null
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function Del(string $token, string $id)
    {
        $request = new request\message\Del($token, $id);
        $response = $this->Req($request, null);

        return $response->result;
    }

}