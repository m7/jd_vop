<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\request\aftersale\AfterSaleToParams;
use jd_vop\request\aftersale\CreateAfsApply;
use jd_vop\request\aftersale\GetAfsOutlinQueryExt;
use jd_vop\request\aftersale\GetGoodsAttributes;
use jd_vop\request\aftersale\UpdateSendInfo;
use jd_vop\response\Response;

/**
 * Class AfterSale
 * @package jd_vop
 */
class AfterSale extends Jd_vop
{

    /**
     * 9.1 查询商品售后属性
     * @param $token string 授权token
     * @param $orderId string 订单号，子订单号
     * @param $customerPin string PIN码，非必填，该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param $wareIds string 商品编码，多个用英文逗号隔开
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetGoodsAttributes($token, $orderId, $customerPin, $wareIds){
        $wareIds = new AfterSaleToParams($wareIds);

        $request = new GetGoodsAttributes($token, $orderId, $customerPin, $wareIds);
        $response = $this->Req($request,\jd_vop\response\aftersale\GetGoodsAttributes::class);
        return $response->result;
    }

    /**
     * 9.2 申请售后
     * @param string $token                     授权token
     * @param int $orderId                      子订单号
     * @param $customerPin                      PIN。必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param string $thirdApplyId              申请批次号，同一子订单下不可重复（长度最大20）
     * @param $isHasInvoice                     是否有发票
     * @param string $customerName              用户名
     * @param string $customerContactName       联系人
     * @param $customerTel                      联系电话，联系电话与手机号不能同时为空
     * @param $customerMobilePhone              手机号，联系电话与手机号不能同时为空
     * @param $customerEmail                    Email
     * @param $customerPostcode                 邮编
     * @param int $pickwareType                 取件方式。4上门取件7客户送货， 40客户发货。
     * @param int $pickWareProvince             取件省
     * @param int $pickWareCity                 取件市
     * @param int $pickWareCounty               取件县
     * @param int $pickWareVillage              取件乡镇
     * @param string $pickWareAddress           取件街道地址
     * @param $reserveDateBegin                 预约取件开始时间。开始时间不可早于当前时间+2小时。格式：2014-09-23 09:00:00
     * @param $reserveDateEnd                   预约取件结束时间。格式：2014-09-23 19:00:00
     * @param int $returnWareType               返件方式。10自营配送（与4取件固定搭配），20第三方配送（与40客户发货固定搭配）
     * @param int $returnWareProvince           返件省
     * @param int $returnWareCity               返件市
     * @param int $returnWareCountry            返件县
     * @param int $returnWareVillage            返件乡镇
     * @param string $returnWareAddress         返件街道地址
     * @param int $customerExpect               客户期望售后类型。10退货，20换货，30维修
     * @param $isNeedDetectionReport            是否需要检测报告
     * @param $lossPreventionTagFlag            是否有防损吊牌
     * @param $isHasPackage                     是否有包装
     * @param $packageDesc                      包装描述
     * @param $questionDesc                     问题描述文字
     * @param $questionPic                      问题描述图片链接。多个图片以“,”分隔
     * @param string $wareId                    商品编号
     * @param string $mainWareId                主商品编号
     * @param string $wareName                  商品名称
     * @param int $wareNum                      商品申请数量
     * @param $wareDescribe                     附件描述
     * @param $payPrice                         支付金额，即“商品单价*数量”
     * @param int $wareType                     商品类型。10主商品，20赠品
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function CreateAfsApply(string $token, int $orderId, $customerPin, string $thirdApplyId, $isHasInvoice, string $customerName, string $customerContactName, $customerTel, $customerMobilePhone, $customerEmail, $customerPostcode, int $pickwareType, int $pickWareProvince, int $pickWareCity, int $pickWareCounty, int $pickWareVillage , string $pickWareAddress, $reserveDateBegin, $reserveDateEnd, int $returnWareType, int $returnWareProvince, int $returnWareCity, int $returnWareCountry, int $returnWareVillage, string $returnWareAddress, int $customerExpect, $isNeedDetectionReport, $lossPreventionTagFlag, $isHasPackage, $packageDesc, $questionDesc, $questionPic, string $wareId, string $mainWareId, string $wareName, int $wareNum, $wareDescribe, $payPrice, int $wareType){
        $request = new CreateAfsApply($token, $orderId, $customerPin, $thirdApplyId, $isHasInvoice, $customerName, $customerContactName, $customerTel, $customerMobilePhone, $customerEmail, $customerPostcode, $pickwareType, $pickWareProvince, $pickWareCity,$pickWareCounty,$pickWareVillage ,$pickWareAddress,$reserveDateBegin,$reserveDateEnd,$returnWareType,$returnWareProvince,$returnWareCity,$returnWareCountry,$returnWareVillage,$returnWareAddress,$customerExpect,$isNeedDetectionReport,$lossPreventionTagFlag,$isHasPackage,$packageDesc,$questionDesc,$questionPic,$wareId,$mainWareId,$wareName,$wareNum,$wareDescribe,$payPrice,$wareType);
        $response = $this->Req($request,\jd_vop\response\aftersale\CreateAfsApply::class);
        return $response->result;
    }

    /**
     * 9.3 查询售后概要
     * @param $token string 授权时获取的access token
     * @param $customerPin string PIN必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param $orderId string 订单号
     * @param $thirdApplyId string 第三方申请单号：thirdApplyId值为申请单编号时，表示特定申请单；thirdApplyId=1时表示选中所有申请单
     * @param $applyPageNo int 第三方申请单号当前页号，默认为 1
     * @param $applyPageSize int 第三方申请每页行数，默认为20 ，max: 100
     * @param $wareId string 商品编号。wareId为SKU编号时，表示特定SKU；wareId=1时表示选中所有SKU
     * @param $warePageNo int 商品维度信息当前页号，默认为 1
     * @param $warePageSize int 商品维度信息每页行数。默认为20 ，最大100
     * @param GetAfsOutlinQueryExt $queryExts
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetAfsOutline($token, $customerPin, $orderId, $thirdApplyId, $applyPageNo, $applyPageSize, $wareId, $warePageNo, $warePageSize, GetAfsOutlinQueryExt $queryExts){
        $request = new request\aftersale\GetAfsOutline($token, $customerPin, $orderId, $thirdApplyId, $applyPageNo, $applyPageSize, $wareId, $warePageNo, $warePageSize,$queryExts);
        $response = $this->Req($request, \jd_vop\response\aftersale\GetAfsOutline::class);
        return $response->result;
    }


    /**
     * 9.4 填写运单信息
     * @param UpdateSendInfo $request
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function UpdateSendInfo(UpdateSendInfo $request){
        $response = $this->Req($request, \jd_vop\response\aftersale\UpdateSendInfo::class);
        return $response->result;
    }
    /**
     * 9.5 确认售后完成
     * @param string $token 授权token
     * @param string $customerPin  PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，子订单号
     * @param string $thirdApplyId 第三方申请单号，集合，多个用英文逗号隔开（举例：15555555,26666666）
     */
    public function ConfirmAfsOrder($toekn ,$customerPin ,$orderId ,$thirdApplyId ){
        $request = new request\aftersale\ConfirmAfsOrder($toekn ,$customerPin ,$orderId ,$thirdApplyId );
        $response = $this->Req($request, \jd_vop\response\aftersale\ConfirmAfsOrder::class);
        return $response->result;
    }

    /**
     * 9.6取消售后申请
     * @param string $token 授权token
     * @param $customerPin  PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，子订单号
     * @param string $thirdApplyId 第三方申请单号
     * @param string $Remark 备注“”可以默认写取消“”
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function CancelAfsApply($token, $customerPin, $orderId, $thirdApplyId, $Remark){
        $request = new request\aftersale\CancelAfsApply($token, $customerPin, $orderId, $thirdApplyId, $Remark);
        $response = $this->Req($request, \jd_vop\response\aftersale\CancelAfsApply::class);
        return $response->result;
    }


    /**
     * 9.7 查询售后申请单明细
     * @param string $token 授权token
     * @param string $customerPin  PIN码，必须是相同合同下的pin。该字段用于验证操作人权限。如果传入账号，则账号必须为订单下单人；如果不传入，则默认可操作主数据合同下所有的订单
     * @param int $orderId 订单号，子订单号
     * @param string $thirdApplyId 第三方申请单号
     * @param string $appendInfoSteps 获取信息模块：1、代表增加获取退款明细、2、代表增加获取客户发货信息；多个用英文逗号隔开（举例：1,2）
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetApplyDetailInfo($toekn , $customerPin , $orderId , $thirdApplyId , $appendInfoSteps ){
        $request = new request\aftersale\GetApplyDetailInfo($toekn ,$customerPin ,$orderId ,$thirdApplyId ,$appendInfoSteps);
        $response = $this->Req($request, \jd_vop\response\aftersale\GetApplyDetailInfo::class);
        return $response->result;
    }

    /**
     * 9.8查询退款明细
     * @param string $token     授权token
     * @param int $orderId      订单号，子订单号
     * @param $refId            售后服务单号
     * @param $pageNo           当前页码，起始页为1
     * @param $pageSize         每页数据条数，不可大于100
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetOrderPayByOrderId($token, $orderId, $refId, $pageNo, $pageSize){
        $request = new request\aftersale\GetOrderPayByOrderId($token, $orderId, $refId, $pageNo, $pageSize);
        $response = $this->Req($request, \jd_vop\response\aftersale\GetOrderPayByOrderId::class);
        return $response->result;
    }




}