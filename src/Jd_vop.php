<?php

namespace jd_vop;

use jd_vop\request\Request;
use jd_vop\response\order\CheckCompleteOrder;
use jd_vop\response\Response;

abstract class Jd_vop
{
    protected $client_id;
    protected $username;
    protected $password_md5;
    protected $client_secret;

    public function __construct($client_id, $username, $password_md5, $client_secret)
    {
        $this->client_id = $client_id;
        $this->username = $username;
        $this->password_md5 = $password_md5;
        $this->client_secret = $client_secret;
    }

    /**
     * 统一请求封装
     * @param Request $request Request 对象  继承自request\Request
     * @param string|null $response response 接收对象  实现 response\Response 接口
     * @return Response
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function Req(Request $request, ?string $response): Response
    {
        $httpResponse = $request->doRequest();
        return new Response($httpResponse, $response);
    }

}