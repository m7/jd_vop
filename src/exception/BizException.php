<?php

namespace jd_vop\exception;


/**
 * 业务侧异常
 */
class BizException extends \Exception
{
    protected $resultMessage;
    protected $resultCode;

    /**
     * 获取业务侧异常信息
     * @return mixed|string
     */
    public function getResultMessage()
    {
        return $this->resultMessage;
    }

    /**
     *  获取业务侧异常错误码
     * @return mixed|string
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }


    public function __construct($resultMessage = "", $resultCode = "")
    {
        $this->resultMessage = $resultMessage;
        $this->resultCode = $resultCode;
        parent::__construct();
    }


}