<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\request\price\GetSellPriceQueryExt;
use jd_vop\response\price\DoPay;
use jd_vop\response\price\GetBalanceDetail;
use jd_vop\response\price\GetSellPrice;
use jd_vop\response\price\GetUnionBalance;

/**
 * Class Price
 * @package jd_vop
 */
class Price extends Jd_vop
{
    /**
     * 5.1 查询商品售卖价
     * @param $token string 授权时获取的access token
     * @param $sku string 商品编号，请以，(英文逗号)分割。例如：129408,129409 (最高支持100个商品)
     * @param $skuInfos mixed [{"skuId":1234,"num":1},{"skuId":1234,"num":1}]，不传时取sku对应的商品编号，数量默认为1。此字段有值则不校验sku字段
     * todo 修改为对象传入
     * @param GetSellPriceQueryExt $queryExts
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetSellPrice(string $token, string $sku, $skuInfos, GetSellPriceQueryExt $queryExts)
    {
        $request = new request\price\GetSellPrice($token, $sku, $skuInfos, $queryExts);
        $response = $this->Req($request, GetSellPrice::class);

        return $response->result;
    }

    /**
     * 8.1 查询余额
     * @param $token string 授权时获取的access token
     * @param string $pin 京东PIN。必须是相同合同下的pin。
     * @param $type string 账户余额类型。
     * 多选，可用英文逗号拼接。
     * 参考枚举值如下： todo 修改枚举
     * 1：账户余额。
     * 2：金采余额。
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetUnionBalance(string $token, string $pin, string $type)
    {
        $request = new request\price\GetUnionBalance($token, $pin, $type);
        $response = $this->Req($request, GetUnionBalance::class);

        return $response->result;
    }

    /**
     * 8.2 查询余额变动明细
     * @param string $token access token
     * @param mixed $pageNum 分页查询当前页数，默认为1
     * @param mixed $pageSize 每页记录数，默认为20
     * @param mixed $orderId 订单号或流水单
     * @param mixed $startDate 开始日期，格式必须：yyyyMMdd
     * @param mixed $endDate 截止日期，格式必须：yyyyMMdd
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetBalanceDetail(string $token, $pageNum, $pageSize, $orderId, $startDate, $endDate)
    {
        $request = new request\price\GetBalanceDetail($token, $pageNum, $pageSize, $orderId, $startDate, $endDate);
        $response = $this->Req($request, GetBalanceDetail::class);

        return $response->result;
    }

    /**
     * 8.3  重新支付接口
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function DoPay(string $token, int $jdOrderId)
    {
        $request = new request\price\DoPay($token, $jdOrderId);
        $response = $this->Req($request, DoPay::class);

        return $response->result;
    }


}