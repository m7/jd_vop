<?php

namespace jd_vop;

use jd_vop\exception\BizException;
use jd_vop\exception\DataException;
use jd_vop\exception\NetWorkException;
use jd_vop\request\stock\GetNewStockById;
use jd_vop\response\Response;

/**
 * Class Stock
 * @package jd_vop
 */
class Stock extends Jd_vop
{
    /**
     * 6.1 查询商品库存
     * @param $token string 授权时获取的access token
     * @param $skuNums string 商品和数量  [{skuId: 569172,num:101}] （ “{skuId: 569172,num:10}”为1条记录，此参数最多传入100条记录 ）
     * @param $area string 格式：13_1000_4277_0 (分别代表1、2、3、4级地址)
     * @return mixed
     * @throws BizException
     * @throws DataException
     * @throws NetWorkException
     */
    public function GetNewStockById($token , $skuNums , $area){
        $request = new GetNewStockById($token ,$skuNums ,$area);
        $response = $this->Req($request,\jd_vop\response\stock\GetNewStockById::class);
        return $response->result;
    }


}