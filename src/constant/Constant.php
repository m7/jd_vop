<?php

namespace jd_vop\constant;
ini_set('date.timezone', 'Asia/Shanghai');

/**
 * 基础常量
 */
interface Constant
{
    /**
     * 京东VOP请求URL
     */
    const BASE_URI = "https://bizapi.jd.com/";
    /**
     * 调用京东接口超时时间
     */
    const REQUEST_TIME_OUT = 30;
}