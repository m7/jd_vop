<?php

namespace jd_vop\constant;
/**
 * 支付方式枚举值
 */
interface PaymentType
{
    /**
     * 货到付款
     */
    const CASH_ON_DELIVERY = 1;

    /**
     * 预存款
     */
    const CHARGE = 4;
    /**
     * 公司转账
     */
    const COMPANY_TRANSFER = 5;
    /**
     * 京东金采
     */
    const JD_JINCAI = 101;

    /**
     * 商城金采
     */
    const MALL_JINCAI = 102;

    /**
     * 混合支付
     */
    const MIXED_PAYMENT = 20;

    const DATA = [
        1 => "货到付款",
        4 => "预存款",
        5 => "公司转账",
        101 => "京东金采",
        102 => "商城金采",
        20 => "为混合支付",
    ];
}